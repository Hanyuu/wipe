/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ui;
import javax.swing.UIManager;
/**
 *
 * @author Hanyuu Furude
 */
public class LookAndFeel
{
    public static void initLookAndFeel(String theme)
	{
		try
		{
                    //UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                    UIManager.setLookAndFeel(theme);
		}
		catch(Exception e)
		{
                    e.printStackTrace();
                    try
                    {
                      UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                    }
                    catch(Exception ee)
                    {
                      ee.printStackTrace();
                    }
		}
	}
}
