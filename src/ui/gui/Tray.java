/*
 * Copyright 2005 Sun Microsystems, Inc. ALL RIGHTS RESERVED 
 * Use of this software is authorized pursuant to the terms of the 
 * license found at http://developers.sun.com/berkeley_license.html
 *
 */

package ui.gui;


import java.awt.*;
import java.awt.event.*;
import ui.interfaces.UI;
import hanyuu.managers.ThreadManager;
import config.Config;
public class Tray implements ui.interfaces.ITray
{
    private SystemTray tray;
    private MenuItem start;
    private MenuItem stop;
    private final PopupMenu popup = new PopupMenu();
    private UI g;
    private final ThreadManager tm;
    private Image image=Toolkit.getDefaultToolkit().getImage(getClass().getResource("/ui/res/tray.png"));
    private TrayIcon trayIcon=new TrayIcon(image, "Ханюша:"+Config.chanName+"\nУспешных постов: 0\nНеудачных постов: 0", popup);

    

    public Tray(ThreadManager t)
    {   
        this.tm=t;
        if (SystemTray.isSupported())
        {

                MouseListener mouseListener = new MouseListener() {
                public void mouseClicked(MouseEvent e)
                {
                  if(e.getButton()==1)
                  {
                    g.setVisible(true);
                    g.seWindowtState(0);
                  }
                           
                }
                public void mouseEntered(MouseEvent e) {
              
                }
                public void mouseExited(MouseEvent e) {
                
                }
                public void mousePressed(MouseEvent e) {
                }
                public void mouseReleased(MouseEvent e) {
                 
                }
            };
            trayIcon.addMouseListener(mouseListener);
            tray = SystemTray.getSystemTray();
            popup.setFont(new Font("Tahoma", Font.BOLD, 11));

            ActionListener exitListener = new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                   System.exit(0);
                }
            };
            ActionListener openListener = new ActionListener() {
                public void actionPerformed(ActionEvent e)
                {
                    g.setVisible(true);
                    g.seWindowtState(0);
                }
            };
            ActionListener StartStopListener = new ActionListener() {
                public void actionPerformed(ActionEvent e)
                {
                    g.SwitchStartStop();
                    try
                    {
                       tm.StartStopManage();

                    }
                    catch(Exception ee)
                    {
                        ee.printStackTrace();
                    }
                    
                    switchState();
                    if(tm.isWork())
                        trayPrint("Ханюша","Запущена");
                    else
                        trayPrint("Ханюша","Остановлена");
                }
            };
            MenuItem exit = new MenuItem("Выйти");
            MenuItem open = new MenuItem("Развернуть");
            start = new MenuItem("Запустить");
            stop = new MenuItem("Остановить");
            exit.addActionListener(exitListener);
            open.addActionListener(openListener);
            start.addActionListener(StartStopListener);
            stop.addActionListener(StartStopListener);
            popup.add(open);
            popup.add(start);
            popup.add(stop);
            popup.add(exit);

            trayIcon.setImageAutoSize(true);
        } 
        else
        {
            System.err.println("System tray is currently not supported.");
        }
    }
             public void addIcon()
             {
                  try
                    {
                      switchState();
                      tray.add(trayIcon);
                    }
                    catch (AWTException e)
                    {
                        System.err.println("TrayIcon could not be added.");
                    }
            }

             public void setImage()
             {
                 trayIcon.setImage(image);
             }
             public void trayPrint(String title,String s)
             {
                 if(Config.dontTrayMsg)
                     return;
                trayIcon.displayMessage(title,s,TrayIcon.MessageType.INFO);
             }
             public void trayPrintError(String title,String s)
             {
                 if(Config.dontTrayMsg)
                     return;
                 trayIcon.displayMessage(title,s,TrayIcon.MessageType.ERROR);
             }
             public void reIcon(int s,int f)
             {
                 trayIcon.setToolTip("Ханюша:"+Config.chanName+"\nУспешных постов: "+s+"\nНеудачных постов: "+f);
             }
             public void switchState()
             {
                 start.setEnabled(!tm.isWork());
                 stop.setEnabled(tm.isWork());
             }
             public void setUI(UI ui)
             {
                 g=ui;
             }
}
