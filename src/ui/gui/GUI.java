/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * GUI.java
 *
 * Created on 01.03.2010, 17:12:45
 */
package ui.gui;

import javax.swing.JOptionPane;
import javax.swing.JFileChooser;
import javax.swing.ImageIcon;
import java.io.InputStream;
import java.io.File;
import javax.imageio.ImageIO;
import java.util.ArrayList;
import javax.swing.text.Document;
import javax.swing.text.StyleConstants;
import javax.swing.text.SimpleAttributeSet;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Color;
import utils.CaptchaUtils;
import javax.swing.table.DefaultTableModel;
import javax.swing.DefaultListModel;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import ui.interfaces.UI;
import hanyuu.ext.interfaces.OCR;
import ui.LookAndFeel;
import ui.other.Faggot;
import hanyuu.net.wipe.AbstractWipe;
import config.WorkMode;
import config.Config;
import hanyuu.net.proxy.HttpProxy;
import hanyuu.managers.ThreadManager;
import hanyuu.ext.ScriptContainer;
import utils.Utils;
/**
 *
 * @author Hanyuu Furude
 */
public final class GUI extends javax.swing.JFrame implements UI {

    private JFileChooser chooser = new JFileChooser();
    private final ThreadManager tm;
    private DefaultListModel pdlm;
    private final static Color green = new Color(0, 153, 51);
    private final static Cursor handCursor = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
    private final static Cursor simpleCursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
    private boolean re = false;
    private Document logDocument;
    private ArrayList<AbstractWipe> wt = new ArrayList<AbstractWipe>();
    private ImageIcon captchaImage;
    private AbstractWipe selectedWipe;
    private boolean fromTray;
    private DefaultTableModel dtm;
    private DefaultListModel sdlm;
    public enum type {

        Info(green),
        Error(Color.RED);
        private final Color kolor;

        type(Color c) {
            this.kolor = c;
        }

        public SimpleAttributeSet getAtributes() {
            SimpleAttributeSet h = new SimpleAttributeSet();
            h.addAttribute(StyleConstants.Foreground, kolor);
            return h;
        }
    }

    /** Creates new form GUI */
    @SuppressWarnings("LeakingThisInConstructor")
    public GUI(ThreadManager tm) {
        LookAndFeel.initLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        this.tm = tm;
        initComponents();
        logDocument = log.getDocument();
        log.setEditable(false);
        log.setBackground(Color.BLACK);
        log.setVisible(true);
        final Dimension screenSize =
                Toolkit.getDefaultToolkit().getScreenSize();
        
        final int x = (screenSize.width - getWidth()) / 2;
        final int y = (screenSize.height - getHeight()) / 2;
        setBounds(x, y, getWidth(), getHeight());
        pdlm = new DefaultListModel();
        sdlm = new DefaultListModel();
        proxys.setModel(pdlm);
        scriptsList.setModel(sdlm);
        /*nextCaptcha.setEnabled(false);
        formerCaptcha.setEnabled(false);
        captchas.setEnabled(false);*/
        suc.setForeground(green);
        failed.setForeground(Color.RED);
        timeout.setToolTipText("Таймаут в милисекундах.");
        hanyuu.setToolTipText("Молись Богине-будь няшкой");
        setUpState(false);
        delSelected.setEnabled(false);
        stopSelected.setEnabled(false);

        if (tm.getChanManager().size() == 0) {
            JOptionPane.showMessageDialog(this, "Нету чанов не могу работать\nХау~", "Ханю", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }

        myInit();
    }
    private void myInit() {
        dtm = (DefaultTableModel) replacements.getModel();
        
        switch(Config.workMode)
        {
            case WipeBoard:
                page.setEnabled(false);
                thread.setEnabled(false);
                modes.setSelectedIndex(0);
                break;
            case Force:
                page.setEnabled(true);
                thread.setEnabled(false);
                modes.setSelectedIndex(1);
                break;
            case OnZeroPage:
                thread.setEnabled(true);
                page.setEnabled(false);
                modes.setSelectedIndex(2);
                break;
            case WipeThread:
                thread.setEnabled(true);
                page.setEnabled(false);
                modes.setSelectedIndex(3);
                break;
        }    
        
        for (String[] s : Config.replacements) {
            dtm.addRow(s);
        }
        randomPicGenerate.setSelected(Config.randomPicGenerate);
        randomize.setSelected(Config.randomizePaste);

        useTmp.setSelected(Config.useTmp);
        colorRndRnd.setSelected(Config.colorRndRnd);  
        colorRnd.setEnabled(!Config.colorRndRnd);
        checkOnLoad.setSelected(Config.checkOnLoad);
        selectFileOrFolder.setEnabled(!Config.BokuNoFile);
        path.setEnabled(!Config.BokuNoFile);
        noFile.setSelected(Config.BokuNoFile);
        syte.setEnabled(Config.contentCheck);
        syteKey.setEnabled(Config.contentCheck);
        page.setValue(Config.page);
        dontEditPixels.setSelected(Config.dontEditPixels);
        checkContent.setSelected(Config.contentCheck);
        silentBump.setSelected(Config.silentBump);
        userAgent.setText(Config.userAgent);
        pixelEdit.setValue(Config.pixelEdit);
        checkOnStart.setSelected(Config.emulateWipe);
        syte.setText(Config.syte);
        syteKey.setText(Config.syteKey);

        if (tm.getImgManager().isPathDir()) {
            picsCount.setText("Всего пикчей в паке: " + String.valueOf(tm.getImgManager().filesCountInDir()));
        }
        password.setText(Config.password);
        path.setText(Config.path);
        rndCount.setValue(Config.rndCount);
        rndCount.setEnabled(Config.randomizePaste);
        one.setSelected(Config.onePic);
        noproxy.setSelected(Config.noProxy);
        setProxyState(!Config.noProxy);
        thread.setText(Config.thread);
        timeout.setValue(Config.timeout);
        message.setText(Config.msg);
        theme.setText(Config.theme);
        board.setText(Config.board);
        useParser.setSelected(Config.parser);
        name.setText(Config.name);
        pasteOnPic.setSelected(Config.pasteOnPic);
        pasteNoMsg.setSelected(Config.pasteNoMsg);
        pasteNoMsg.setEnabled(Config.pasteOnPic);
        pasteNoMsg.setEnabled(Config.pasteOnPic);
        picsTxtR.setEnabled(Config.pasteOnPic);
        picsTxtG.setEnabled(Config.pasteOnPic);
        picsTxtB.setEnabled(Config.pasteOnPic);
        dontEditPixels.setEnabled(Config.dontEditPixels);
        threads.setValue(Config.threads);
        colorRnd.setValue(Config.colorRnd);
        setPasteCount(tm.getCopyPasteManager().size());
        empty.setSelected(Config.emptySeparator);
        myseparator.setSelected(!Config.emptySeparator);
        symbol.setEnabled(!Config.emptySeparator);
        symbol.setText(Config.separator);
        randomize.setSelected(Config.randomizePaste);
        msgCount.setSelected(Config.msgCount);
        msgCountInteger.setValue(Config.msgCountInt);
        msgCountInteger.setEnabled(Config.msgCount);
        picsPuck.setSelected(Config.picsPuck);
        dontEdit.setSelected(Config.picsNotEdit);
        maxFileSize.setValue(Config.maxFileSize);
        dontEdit.setEnabled(!Config.BokuNoFile);
        picsPuck.setEnabled(!Config.BokuNoFile);
        picsCount.setEnabled(!Config.BokuNoFile);
        maxFileSize.setEnabled(!Config.BokuNoFile);
        setPicsEnable(!Config.BokuNoFile && !Config.picsNotEdit);
        picsTxtR.setValue(Config.picsTxtR);
        picsTxtG.setValue(Config.picsTxtG);
        picsTxtB.setValue(Config.picsTxtB);
        startX.setValue(Config.startX);
        startY.setValue(Config.startY);
        deltaY.setValue(Config.deltaY);
        FontName.setText(Config.FontName);
        fontSize.setValue(Config.fontSize);
        randomBytes.setSelected(Config.randomBytes);
        randomBytes.setEnabled(Config.useTmp);
        dontTrayMsg.setSelected(Config.dontTrayMsg);
        email.setText(Config.email);
        key.setText(Config.keyForServices);
        scaleW.setText(String.valueOf(Config.scaleW));
        scaleH.setText(String.valueOf(Config.scaleH));
        silentBump.setEnabled(Config.parser);
        reverseCaptcha.setSelected(Config.reverseCaptcha);
        smartErrorHandler.setSelected(Config.smartErrorHandler);
        smartErrorCount.setEnabled(Config.smartErrorHandler);
        smartErrorCount.setText(String.valueOf(Config.smartErrorCount));
        smartErrorAction.setEnabled(Config.smartErrorHandler);
        smartErrorAction.setSelectedIndex(Config.smartErrorAction);
        waitForNotReady.setSelected(Config.waitForNotReady);
        message.setText(Config.msg);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel31 = new javax.swing.JLabel();
        mainPane = new javax.swing.JTabbedPane();
        main = new javax.swing.JPanel();
        start = new javax.swing.JButton();
        hanyuu = new javax.swing.JLabel();
        captchas = new javax.swing.JComboBox();
        cimg = new javax.swing.JLabel();
        ctxt = new javax.swing.JTextField();
        reMe = new javax.swing.JButton();
        send = new javax.swing.JButton();
        reAll = new javax.swing.JButton();
        status = new javax.swing.JLabel();
        suc = new javax.swing.JLabel();
        failed = new javax.swing.JLabel();
        ontop = new javax.swing.JCheckBox();
        runThreads = new javax.swing.JLabel();
        nextCaptcha = new javax.swing.JButton();
        formerCaptcha = new javax.swing.JButton();
        delSelected = new javax.swing.JButton();
        stopSelected = new javax.swing.JButton();
        dellAll = new javax.swing.JButton();
        logScroll = new javax.swing.JScrollPane();
        log = new javax.swing.JTextPane();
        dontJamp = new javax.swing.JCheckBox();
        jButton1 = new javax.swing.JButton();
        settings = new javax.swing.JPanel();
        chans = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        page = new javax.swing.JSpinner();
        jLabel7 = new javax.swing.JLabel();
        board = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        timeout = new javax.swing.JSpinner();
        thread = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        threads = new javax.swing.JSpinner();
        silentBump = new javax.swing.JCheckBox();
        msgCountInteger = new javax.swing.JSpinner();
        msgCount = new javax.swing.JCheckBox();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        userAgent = new javax.swing.JTextField();
        key = new javax.swing.JTextField();
        ocrName = new javax.swing.JLabel();
        ocrs = new javax.swing.JComboBox();
        modes = new javax.swing.JComboBox();
        useParser = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        scaleW = new javax.swing.JTextField();
        scaleH = new javax.swing.JTextField();
        reverseCaptcha = new javax.swing.JCheckBox();
        dontTrayMsg = new javax.swing.JCheckBox();
        smartErrorHandler = new javax.swing.JCheckBox();
        smartErrorCount = new javax.swing.JTextField();
        smartErrorAction = new javax.swing.JComboBox();
        jLabel35 = new javax.swing.JLabel();
        waitForNotReady = new javax.swing.JCheckBox();
        proxy = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        proxys = new javax.swing.JList();
        check = new javax.swing.JButton();
        checkAll = new javax.swing.JButton();
        delete = new javax.swing.JButton();
        checkOnLoad = new javax.swing.JCheckBox();
        checkContent = new javax.swing.JCheckBox();
        noproxy = new javax.swing.JCheckBox();
        load = new javax.swing.JButton();
        checkOnStart = new javax.swing.JCheckBox();
        syte = new javax.swing.JTextField();
        syteKey = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        proxyProgress = new javax.swing.JProgressBar();
        paste = new javax.swing.JPanel();
        all = new javax.swing.JLabel();
        reload = new javax.swing.JButton();
        empty = new javax.swing.JRadioButton();
        myseparator = new javax.swing.JRadioButton();
        symbol = new javax.swing.JTextField();
        randomize = new javax.swing.JCheckBox();
        fromfile = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        rndCount = new javax.swing.JSpinner();
        jLabel17 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        replacements = new javax.swing.JTable();
        AddRrow = new javax.swing.JButton();
        RemoveRrow = new javax.swing.JButton();
        saver = new javax.swing.JButton();
        theme = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        message = new javax.swing.JTextPane();
        name = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        password = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        email = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        pics = new javax.swing.JPanel();
        path = new javax.swing.JTextField();
        selectFileOrFolder = new javax.swing.JButton();
        noFile = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        picsPuck = new javax.swing.JCheckBox();
        dontEdit = new javax.swing.JCheckBox();
        picsCount = new javax.swing.JLabel();
        one = new javax.swing.JCheckBox();
        useTmp = new javax.swing.JCheckBox();
        maxFileSize = new javax.swing.JSpinner();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        colorRnd = new javax.swing.JSpinner();
        jLabel20 = new javax.swing.JLabel();
        pixelEdit = new javax.swing.JSpinner();
        pasteOnPic = new javax.swing.JCheckBox();
        dontEditPixels = new javax.swing.JCheckBox();
        pasteNoMsg = new javax.swing.JCheckBox();
        picsTxtR = new javax.swing.JSpinner();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        picsTxtG = new javax.swing.JSpinner();
        jLabel23 = new javax.swing.JLabel();
        picsTxtB = new javax.swing.JSpinner();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        startX = new javax.swing.JSpinner();
        startY = new javax.swing.JSpinner();
        colorRndRnd = new javax.swing.JCheckBox();
        jLabel28 = new javax.swing.JLabel();
        deltaY = new javax.swing.JSpinner();
        FontName = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        fontSize = new javax.swing.JSpinner();
        jLabel30 = new javax.swing.JLabel();
        randomBytes = new javax.swing.JCheckBox();
        randomPicGenerate = new javax.swing.JCheckBox();
        scripts = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        scriptsList = new javax.swing.JList();
        reLoad = new javax.swing.JButton();
        shutdown = new javax.swing.JButton();
        loadScript = new javax.swing.JButton();
        scriptInfo = new javax.swing.JLabel();
        dontUseScript = new javax.swing.JCheckBox();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jLabel6.setText("jLabel6");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jLabel31.setText("jLabel31");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Рогатулечка");
        setBackground(new java.awt.Color(0, 0, 0));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                formFocusGained(evt);
            }
        });
        addWindowStateListener(new java.awt.event.WindowStateListener() {
            public void windowStateChanged(java.awt.event.WindowEvent evt) {
                formWindowStateChanged(evt);
            }
        });

        main.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mainMouseClicked(evt);
            }
        });
        main.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                mainFocusLost(evt);
            }
        });
        main.setLayout(null);

        start.setText("Запустить");
        start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startActionPerformed(evt);
            }
        });
        main.add(start);
        start.setBounds(440, 230, 121, 23);

        hanyuu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ui/res/h.png"))); // NOI18N
        hanyuu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                hanyuuMouseClicked(evt);
            }
        });
        main.add(hanyuu);
        hanyuu.setBounds(0, 0, 100, 131);

        captchas.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                captchasItemStateChanged(evt);
            }
        });
        main.add(captchas);
        captchas.setBounds(131, 76, 190, 22);

        cimg.setMaximumSize(new java.awt.Dimension(999, 999));
        cimg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cimgMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                cimgMouseEntered(evt);
            }
        });
        main.add(cimg);
        cimg.setBounds(327, 11, 267, 97);

        ctxt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ctxtKeyPressed(evt);
            }
        });
        main.add(ctxt);
        ctxt.setBounds(327, 148, 270, 20);

        reMe.setText("Обновить");
        reMe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reMeActionPerformed(evt);
            }
        });
        main.add(reMe);
        reMe.setBounds(125, 147, 196, 23);

        send.setText("Послать");
        send.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendActionPerformed(evt);
            }
        });
        main.add(send);
        send.setBounds(410, 180, 102, 23);

        reAll.setText("Обновить все");
        reAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reAllActionPerformed(evt);
            }
        });
        main.add(reAll);
        reAll.setBounds(125, 117, 196, 23);
        main.add(status);
        status.setBounds(330, 210, 260, 14);

        suc.setText("Всего успешно:   0");
        main.add(suc);
        suc.setBounds(200, 180, 119, 14);

        failed.setText("Всего неудачно: 0");
        main.add(failed);
        failed.setBounds(200, 200, 119, 14);

        ontop.setText("Поверх всех окон");
        ontop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ontopActionPerformed(evt);
            }
        });
        main.add(ontop);
        ontop.setBounds(10, 180, 115, 23);
        main.add(runThreads);
        runThreads.setBounds(0, 260, 640, 30);

        nextCaptcha.setText(">");
        nextCaptcha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextCaptchaActionPerformed(evt);
            }
        });
        main.add(nextCaptcha);
        nextCaptcha.setBounds(550, 180, 43, 23);

        formerCaptcha.setText("<");
        formerCaptcha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                formerCaptchaActionPerformed(evt);
            }
        });
        main.add(formerCaptcha);
        formerCaptcha.setBounds(330, 180, 43, 23);

        delSelected.setText("Удалить");
        delSelected.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delSelectedActionPerformed(evt);
            }
        });
        main.add(delSelected);
        delSelected.setBounds(10, 230, 100, 23);

        stopSelected.setText("Остановить");
        stopSelected.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopSelectedActionPerformed(evt);
            }
        });
        main.add(stopSelected);
        stopSelected.setBounds(120, 230, 111, 23);

        dellAll.setText("Удалить все посты");
        dellAll.setEnabled(false);
        dellAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dellAllActionPerformed(evt);
            }
        });
        main.add(dellAll);
        dellAll.setBounds(240, 230, 170, 23);

        logScroll.setViewportView(log);

        main.add(logScroll);
        logScroll.setBounds(0, 290, 710, 150);

        dontJamp.setText("Не выпрыгивать из трея");
        main.add(dontJamp);
        dontJamp.setBounds(10, 200, 151, 23);

        jButton1.setText("Запустить потоки");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        main.add(jButton1);
        jButton1.setBounds(570, 230, 130, 23);

        mainPane.addTab("Ханюша", main);

        chans.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chansActionPerformed(evt);
            }
        });

        jLabel5.setText("Брать треды с:");

        page.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                pageStateChanged(evt);
            }
        });
        page.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pageKeyPressed(evt);
            }
        });

        jLabel7.setText("страницы");

        board.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                boardFocusLost(evt);
            }
        });

        jLabel13.setText("Доска:");

        jLabel8.setText("Потоков:");

        timeout.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                timeoutStateChanged(evt);
            }
        });
        timeout.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                timeoutKeyPressed(evt);
            }
        });

        thread.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                threadKeyPressed(evt);
            }
        });

        jLabel9.setText("Тред:");

        jLabel12.setText("Таймаут:");

        threads.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                threadsStateChanged(evt);
            }
        });
        threads.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                threadsKeyPressed(evt);
            }
        });

        silentBump.setText("Удалять пост(тихий бамп)");
        silentBump.setEnabled(false);
        silentBump.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                silentBumpActionPerformed(evt);
            }
        });

        msgCountInteger.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                msgCountIntegerStateChanged(evt);
            }
        });
        msgCountInteger.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                msgCountIntegerKeyPressed(evt);
            }
        });

        msgCount.setText("Не более");
        msgCount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                msgCountMouseClicked(evt);
            }
        });

        jLabel15.setText("Сообщений");

        jLabel16.setText("UserAgent:");

        userAgent.setMaximumSize(new java.awt.Dimension(6, 20));
        userAgent.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                userAgentFocusLost(evt);
            }
        });

        key.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                keyMouseClicked(evt);
            }
        });
        key.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                keyActionPerformed(evt);
            }
        });
        key.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                keyFocusLost(evt);
            }
        });
        key.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                keyKeyTyped(evt);
            }
        });

        ocrName.setText("Ключ:");

        ocrs.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ocrsItemStateChanged(evt);
            }
        });

        modes.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Вайп", "Форс", "Держать на нулевой", "В тред" }));
        modes.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                modesItemStateChanged(evt);
            }
        });

        useParser.setText("Использовать парсер.");
        useParser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useParserActionPerformed(evt);
            }
        });

        jLabel4.setText("Масштабирование капчи");

        jLabel33.setText("Width*");

        jLabel34.setText("Height*");

        scaleW.setText("1");
        scaleW.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                scaleWFocusLost(evt);
            }
        });

        scaleH.setText("1");
        scaleH.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                scaleHFocusLost(evt);
            }
        });

        reverseCaptcha.setText("Обратить цвета капчи на противоположные");
        reverseCaptcha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reverseCaptchaActionPerformed(evt);
            }
        });

        dontTrayMsg.setText("Не выводить сообщения в трее");
        dontTrayMsg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dontTrayMsgActionPerformed(evt);
            }
        });

        smartErrorHandler.setText("После");
        smartErrorHandler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                smartErrorHandlerActionPerformed(evt);
            }
        });

        smartErrorCount.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                smartErrorCountFocusLost(evt);
            }
        });

        smartErrorAction.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Выключать поток", "Перезапускать поток" }));
        smartErrorAction.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                smartErrorActionItemStateChanged(evt);
            }
        });

        jLabel35.setText("ошибок");

        waitForNotReady.setText("Ждать всех");
        waitForNotReady.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                waitForNotReadyActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout settingsLayout = new javax.swing.GroupLayout(settings);
        settings.setLayout(settingsLayout);
        settingsLayout.setHorizontalGroup(
            settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(settingsLayout.createSequentialGroup()
                .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(214, 214, 214)
                        .addComponent(ocrs, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(useParser, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(silentBump)
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(4, 4, 4)
                                .addComponent(page, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(jLabel7)))
                        .addGap(29, 29, 29)
                        .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(modes, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chans, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addComponent(msgCount)
                                .addGap(2, 2, 2)
                                .addComponent(msgCountInteger, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addGap(4, 4, 4)
                                .addComponent(board, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(10, 10, 10)
                        .addComponent(jLabel15))
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, settingsLayout.createSequentialGroup()
                                        .addComponent(smartErrorHandler)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(smartErrorCount, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel35)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(smartErrorAction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, settingsLayout.createSequentialGroup()
                                        .addComponent(jLabel33)
                                        .addGap(4, 4, 4)
                                        .addComponent(scaleW, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(4, 4, 4)
                                        .addComponent(jLabel34)
                                        .addGap(4, 4, 4)
                                        .addComponent(scaleH, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(reverseCaptcha))
                                    .addComponent(waitForNotReady, javax.swing.GroupLayout.Alignment.LEADING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dontTrayMsg))))
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addGap(4, 4, 4)
                                .addComponent(userAgent, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(timeout)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(threads, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, settingsLayout.createSequentialGroup()
                                .addComponent(ocrName)
                                .addGap(27, 27, 27)
                                .addComponent(key, javax.swing.GroupLayout.PREFERRED_SIZE, 278, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(jLabel9)
                                .addGap(18, 18, 18)
                                .addComponent(thread, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(68, Short.MAX_VALUE))
        );
        settingsLayout.setVerticalGroup(
            settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(settingsLayout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(ocrs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addComponent(useParser)
                        .addComponent(silentBump)
                        .addGap(5, 5, 5)
                        .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jLabel5))
                            .addComponent(page, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jLabel7))))
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(modes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(chans, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(msgCount)
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(msgCountInteger, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(6, 6, 6)
                        .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabel13))
                            .addComponent(board, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel15)))
                .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabel16))
                            .addComponent(userAgent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabel12))
                            .addGroup(settingsLayout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(threads, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel8)))))
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(timeout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(6, 6, 6)
                .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(ocrName))
                    .addComponent(key, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel9))
                    .addComponent(thread, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addComponent(jLabel4)
                .addGap(5, 5, 5)
                .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel33))
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(scaleW, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel34))
                    .addGroup(settingsLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(scaleH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(reverseCaptcha)
                            .addComponent(dontTrayMsg))))
                .addGap(18, 18, 18)
                .addGroup(settingsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(smartErrorHandler)
                    .addComponent(smartErrorCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel35)
                    .addComponent(smartErrorAction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(waitForNotReady)
                .addContainerGap(107, Short.MAX_VALUE))
        );

        mainPane.addTab("Общие настройки", settings);

        jScrollPane1.setViewportView(proxys);

        check.setText("Проверить выделенную");
        check.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkActionPerformed(evt);
            }
        });

        checkAll.setText("Проверить все");
        checkAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkAllActionPerformed(evt);
            }
        });

        delete.setText("Удалить выделенные");
        delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteActionPerformed(evt);
            }
        });

        checkOnLoad.setText("Проверять прокси при запуске программы");
        checkOnLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkOnLoadActionPerformed(evt);
            }
        });

        checkContent.setText("Проверять передаваемый контент");
        checkContent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkContentActionPerformed(evt);
            }
        });

        noproxy.setText("Без прокси");
        noproxy.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noproxyActionPerformed(evt);
            }
        });

        load.setText("Взять из файла");
        load.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadActionPerformed(evt);
            }
        });

        checkOnStart.setText("Эмулировать вайп");
        checkOnStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkOnStartActionPerformed(evt);
            }
        });

        syte.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                syteFocusLost(evt);
            }
        });
        syte.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                syteKeyTyped(evt);
            }
        });

        syteKey.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                syteKeyFocusLost(evt);
            }
        });
        syteKey.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                syteKeyKeyTyped(evt);
            }
        });

        jLabel1.setText("Сайт для запроса контента:");

        jLabel32.setText("Ответ должен содержать:");

        javax.swing.GroupLayout proxyLayout = new javax.swing.GroupLayout(proxy);
        proxy.setLayout(proxyLayout);
        proxyLayout.setHorizontalGroup(
            proxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(proxyLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(proxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(checkOnStart)
                    .addGroup(proxyLayout.createSequentialGroup()
                        .addComponent(noproxy)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(load))
                    .addGroup(proxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(checkContent, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(checkOnLoad, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(proxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(proxyProgress, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, proxyLayout.createSequentialGroup()
                            .addGroup(proxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(checkAll, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(check, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                                .addComponent(delete, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(proxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel1)
                                .addComponent(syte)
                                .addComponent(syteKey)
                                .addComponent(jLabel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        proxyLayout.setVerticalGroup(
            proxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(proxyLayout.createSequentialGroup()
                .addGroup(proxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(proxyLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(check)
                        .addGap(18, 18, 18))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, proxyLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(5, 5, 5)))
                .addGroup(proxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(checkAll)
                    .addComponent(syte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addComponent(jLabel32)
                .addGap(2, 2, 2)
                .addGroup(proxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(delete)
                    .addComponent(syteKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(checkOnLoad)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkContent)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(proxyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(noproxy)
                    .addComponent(load))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(checkOnStart)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(proxyProgress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(142, 142, 142))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 442, Short.MAX_VALUE)
        );

        mainPane.addTab("Прокси", proxy);

        all.setText("Всего копипасты: 0");

        reload.setText("Перезагрузить");
        reload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reloadActionPerformed(evt);
            }
        });

        empty.setText("Пустя строка");
        empty.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                emptyMouseClicked(evt);
            }
        });

        myseparator.setText("Свой символ");
        myseparator.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                myseparatorMouseClicked(evt);
            }
        });

        symbol.setText("-=-=-=-");
        symbol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                symbolActionPerformed(evt);
            }
        });
        symbol.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                symbolFocusLost(evt);
            }
        });

        randomize.setText("Рандомизировать");
        randomize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                randomizeActionPerformed(evt);
            }
        });

        fromfile.setText("Из файла...");
        fromfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fromfileActionPerformed(evt);
            }
        });

        jLabel14.setText("Разделитель:");

        rndCount.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                rndCountStateChanged(evt);
            }
        });
        rndCount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                rndCountKeyTyped(evt);
            }
        });

        jLabel17.setText("Степень рандомизции:");

        replacements.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Что заменяем", "На что заменяем"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane4.setViewportView(replacements);

        AddRrow.setText("Добавить строку");
        AddRrow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddRrowActionPerformed(evt);
            }
        });

        RemoveRrow.setText("Удалить строку");
        RemoveRrow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RemoveRrowActionPerformed(evt);
            }
        });

        saver.setText("Сохранить замены");
        saver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saverActionPerformed(evt);
            }
        });

        theme.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                themeFocusLost(evt);
            }
        });

        jLabel11.setText("Тема:");

        message.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                messageFocusLost(evt);
            }
        });
        jScrollPane2.setViewportView(message);

        name.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                nameFocusLost(evt);
            }
        });

        jLabel10.setText("Имя:");

        password.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                passwordFocusLost(evt);
            }
        });

        jLabel3.setText("Пароль:");

        email.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                emailFocusLost(evt);
            }
        });

        jLabel36.setText("Email:");

        javax.swing.GroupLayout pasteLayout = new javax.swing.GroupLayout(paste);
        paste.setLayout(pasteLayout);
        pasteLayout.setHorizontalGroup(
            pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pasteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pasteLayout.createSequentialGroup()
                        .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel10)
                            .addComponent(jLabel3)
                            .addComponent(jLabel36))
                        .addGap(18, 18, 18)
                        .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(email, javax.swing.GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
                            .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(password)
                                .addComponent(theme)
                                .addComponent(name, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE))))
                    .addComponent(randomize)
                    .addGroup(pasteLayout.createSequentialGroup()
                        .addComponent(myseparator)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(symbol, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(all)
                    .addGroup(pasteLayout.createSequentialGroup()
                        .addComponent(reload)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fromfile))
                    .addComponent(empty)
                    .addComponent(jLabel14)
                    .addGroup(pasteLayout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rndCount, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pasteLayout.createSequentialGroup()
                        .addComponent(RemoveRrow, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(AddRrow)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(saver))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 463, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 711, Short.MAX_VALUE)
        );
        pasteLayout.setVerticalGroup(
            pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pasteLayout.createSequentialGroup()
                .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pasteLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(all)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(reload)
                            .addComponent(fromfile))
                        .addGap(5, 5, 5)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(empty)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(myseparator)
                            .addComponent(symbol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(randomize)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(rndCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(16, 16, 16)
                        .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(theme, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addGap(10, 10, 10)
                        .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(pasteLayout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(RemoveRrow)
                            .addComponent(AddRrow)
                            .addComponent(saver))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pasteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel36))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE))
        );

        mainPane.addTab("Текст", paste);

        path.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pathMouseClicked(evt);
            }
        });
        path.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pathKeyPressed(evt);
            }
        });

        selectFileOrFolder.setText("...");
        selectFileOrFolder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectFileOrFolderActionPerformed(evt);
            }
        });

        noFile.setText("Без файла");
        noFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noFileActionPerformed(evt);
            }
        });

        jLabel2.setText("Папка с пикчами:");

        picsPuck.setText("Не повторять пикчи");
        picsPuck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                picsPuckActionPerformed(evt);
            }
        });

        dontEdit.setText("Не изменять пикчи");
        dontEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dontEditActionPerformed(evt);
            }
        });

        picsCount.setText("Всего пикчей в паке: ");

        one.setText("постить одну картинку");
        one.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                oneStateChanged(evt);
            }
        });
        one.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                oneActionPerformed(evt);
            }
        });

        useTmp.setText("постить через tmp");
        useTmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useTmpActionPerformed(evt);
            }
        });

        maxFileSize.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                maxFileSizeStateChanged(evt);
            }
        });
        maxFileSize.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                maxFileSizeKeyTyped(evt);
            }
        });

        jLabel18.setText("Максимальный размер в байтах:");

        jLabel19.setText("Отклонение по цвету:");

        colorRnd.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                colorRndStateChanged(evt);
            }
        });
        colorRnd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                colorRndKeyTyped(evt);
            }
        });

        jLabel20.setText("Сколько пикселей менять:");

        pixelEdit.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                pixelEditStateChanged(evt);
            }
        });
        pixelEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                pixelEditKeyTyped(evt);
            }
        });

        pasteOnPic.setText("Писать текст сообщения на пикчах");
        pasteOnPic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pasteOnPicActionPerformed(evt);
            }
        });

        dontEditPixels.setText("Не изменять пиксели");
        dontEditPixels.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dontEditPixelsActionPerformed(evt);
            }
        });

        pasteNoMsg.setText("Не слать текстовые сообщения");
        pasteNoMsg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pasteNoMsgActionPerformed(evt);
            }
        });

        picsTxtR.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                picsTxtRStateChanged(evt);
            }
        });
        picsTxtR.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                picsTxtRKeyTyped(evt);
            }
        });

        jLabel21.setText("R:");

        jLabel22.setText("G:");

        picsTxtG.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                picsTxtGStateChanged(evt);
            }
        });
        picsTxtG.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                picsTxtGKeyTyped(evt);
            }
        });

        jLabel23.setText("B:");

        picsTxtB.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                picsTxtBStateChanged(evt);
            }
        });
        picsTxtB.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                picsTxtBKeyTyped(evt);
            }
        });

        jLabel24.setText("Цвет надписи.");

        jLabel25.setText("Наносить текст с:");

        jLabel26.setText("X:");

        jLabel27.setText("Y:");

        startX.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                startXStateChanged(evt);
            }
        });
        startX.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                startXKeyTyped(evt);
            }
        });

        startY.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                startYStateChanged(evt);
            }
        });
        startY.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                startYKeyTyped(evt);
            }
        });

        colorRndRnd.setText("Случайно");
        colorRndRnd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                colorRndRndActionPerformed(evt);
            }
        });

        jLabel28.setText("Межстрочное расстояние:");

        deltaY.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                deltaYStateChanged(evt);
            }
        });
        deltaY.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                deltaYKeyTyped(evt);
            }
        });

        FontName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                FontNameFocusLost(evt);
            }
        });
        FontName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                FontNameKeyTyped(evt);
            }
        });

        jLabel29.setText("Размер шрифта:");

        fontSize.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                fontSizeStateChanged(evt);
            }
        });
        fontSize.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                fontSizeKeyTyped(evt);
            }
        });

        jLabel30.setText("Название Шрифта:");

        randomBytes.setText("Случайные байты");
        randomBytes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                randomBytesActionPerformed(evt);
            }
        });

        randomPicGenerate.setText("random pic");
        randomPicGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                randomPicGenerateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout picsLayout = new javax.swing.GroupLayout(pics);
        pics.setLayout(picsLayout);
        picsLayout.setHorizontalGroup(
            picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(picsLayout.createSequentialGroup()
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(picsLayout.createSequentialGroup()
                                .addGap(90, 90, 90)
                                .addComponent(path, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(8, 8, 8)
                        .addComponent(selectFileOrFolder, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(noFile)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(randomPicGenerate))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(dontEdit)
                        .addGap(25, 25, 25)
                        .addComponent(picsCount, javax.swing.GroupLayout.PREFERRED_SIZE, 403, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(picsPuck)
                        .addGap(38, 38, 38)
                        .addComponent(dontEditPixels))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(one)
                        .addGap(22, 22, 22)
                        .addComponent(useTmp))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel18)
                        .addGap(10, 10, 10)
                        .addComponent(maxFileSize, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel19)
                        .addGap(10, 10, 10)
                        .addComponent(colorRnd, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(11, 11, 11)
                        .addComponent(colorRndRnd)
                        .addGap(6, 6, 6)
                        .addComponent(randomBytes))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel20)
                        .addGap(4, 4, 4)
                        .addComponent(pixelEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(picsLayout.createSequentialGroup()
                                .addGap(101, 101, 101)
                                .addComponent(FontName, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(pasteOnPic)
                        .addComponent(pasteNoMsg))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel21)
                        .addGap(4, 4, 4)
                        .addComponent(picsTxtR, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(jLabel22)
                        .addGap(4, 4, 4)
                        .addComponent(picsTxtG, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(jLabel23)
                        .addGap(4, 4, 4)
                        .addComponent(picsTxtB, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(jLabel24)
                        .addGap(6, 6, 6)
                        .addComponent(jLabel29)
                        .addGap(4, 4, 4)
                        .addComponent(fontSize, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel25))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(picsLayout.createSequentialGroup()
                                .addComponent(jLabel27)
                                .addGap(10, 10, 10)
                                .addComponent(startY))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, picsLayout.createSequentialGroup()
                                .addComponent(jLabel26)
                                .addGap(10, 10, 10)
                                .addComponent(startX, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(30, 30, 30)
                        .addComponent(jLabel28)
                        .addGap(16, 16, 16)
                        .addComponent(deltaY, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(145, Short.MAX_VALUE))
        );
        picsLayout.setVerticalGroup(
            picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(picsLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(jLabel2))
                    .addComponent(path, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(selectFileOrFolder)
                    .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(noFile)
                        .addComponent(randomPicGenerate)))
                .addGap(7, 7, 7)
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dontEdit)
                    .addComponent(picsCount))
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(picsPuck)
                    .addComponent(dontEditPixels))
                .addGap(2, 2, 2)
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(one)
                    .addComponent(useTmp))
                .addGap(6, 6, 6)
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel18))
                    .addComponent(maxFileSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel19))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(colorRnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(colorRndRnd)
                    .addComponent(randomBytes))
                .addGap(27, 27, 27)
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel20))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(pixelEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel30))
                    .addComponent(FontName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pasteOnPic)
                    .addComponent(pasteNoMsg))
                .addGap(2, 2, 2)
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel21))
                    .addComponent(picsTxtR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel22))
                    .addComponent(picsTxtG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel23))
                    .addComponent(picsTxtB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel24))
                    .addGroup(picsLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel29))
                    .addComponent(fontSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(jLabel25)
                .addGap(11, 11, 11)
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel26)
                    .addComponent(startX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28)
                    .addComponent(deltaY, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(picsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel27)
                    .addComponent(startY, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        mainPane.addTab("Пикчи", pics);

        scriptsList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                scriptsListMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(scriptsList);

        reLoad.setText("Перезагрузить");
        reLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reLoadActionPerformed(evt);
            }
        });

        shutdown.setText("Выгрузить");
        shutdown.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                shutdownActionPerformed(evt);
            }
        });

        loadScript.setText("Загрузить");
        loadScript.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadScriptActionPerformed(evt);
            }
        });

        dontUseScript.setText("Не использовать.");
        dontUseScript.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dontUseScriptActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout scriptsLayout = new javax.swing.GroupLayout(scripts);
        scripts.setLayout(scriptsLayout);
        scriptsLayout.setHorizontalGroup(
            scriptsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(scriptsLayout.createSequentialGroup()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(scriptsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(scriptsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(loadScript, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(shutdown, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(reLoad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(scriptInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 317, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dontUseScript))
                .addContainerGap(99, Short.MAX_VALUE))
        );
        scriptsLayout.setVerticalGroup(
            scriptsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 442, Short.MAX_VALUE)
            .addGroup(scriptsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(reLoad)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(shutdown)
                .addGap(18, 18, 18)
                .addComponent(loadScript)
                .addGap(26, 26, 26)
                .addComponent(dontUseScript)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 249, Short.MAX_VALUE)
                .addComponent(scriptInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        mainPane.addTab("Скрипты", scripts);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPane, javax.swing.GroupLayout.DEFAULT_SIZE, 716, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mainPane, javax.swing.GroupLayout.PREFERRED_SIZE, 467, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    public void reSelectOCR()
    {
        ocrs.setSelectedItem(tm.getScripts().getOCRbyName(Config.ocrMode));
    }
    public void addChan(String name) {
        chans.addItem(name);
    }

    public void showMessage(String msg,int type)
    {
        JOptionPane.showMessageDialog(this, msg, "Ханю", type);
    }
    public void openPane(int p) {
        mainPane.setSelectedIndex(p);
    }

    public void setPasteCount(int paste) {
        all.setText("Всего копипасты: " + String.valueOf(paste));
    }

    public void setSuccessful(int i) {
        if(Config.parser)
            dellAll.setEnabled(i>0);
        suc.setText("Всего успешно:   " + String.valueOf(i));
    }

    public void setFailed(int i) {
        failed.setText("Всего неудачно: " + String.valueOf(i));
    }

    public void setThreadState(boolean state) {
        thread.setEnabled(state);
    }

    @Override
    @SuppressWarnings("CallToThreadDumpStack")
    public void logInfo(String text) {
        try {
            logDocument.insertString(logDocument.getLength(), "[" + Utils.getTime(":") + "]" + text + "\n", type.Info.getAtributes());        
            log.setCaretPosition(logDocument.getLength());
        } catch (Exception e) {
            //Ignore problems;
        }
    }

    @Override
    @SuppressWarnings("CallToThreadDumpStack")
    public void logError(String text) {
        try {
            logDocument.insertString(logDocument.getLength(), "[" + Utils.getTime(":") + "]" + text + "\n", type.Error.getAtributes());
            log.setCaretPosition(logDocument.getLength());
        } catch (Exception e) {

        }
    }

    @Override
    public int getThreads() {
        return (Integer) threads.getValue();
    }

    @Override
    public void SwitchStartStop() {
        if (tm.isWork()) {
            start();
            setState(!tm.isWork());
            setUpState(!tm.isWork());
        } else {
            stop();
            setUpState(tm.isWork());
            setState(!tm.isWork());
        }
    }

    private void setState(boolean w) {
        suc.setVisible(!w);
        failed.setVisible(!w);
        message.setEnabled(w);
        mainPane.setEnabledAt(1, w);
        mainPane.setEnabledAt(2, w);
        mainPane.setEnabledAt(3, w);
        mainPane.setEnabledAt(4, w);
        mainPane.setEnabledAt(5, w);
    }

    private void start() {

        Config.threads = (Integer) threads.getValue();
        if (!checkThreads()) {
            return;
        }

        Config.saveConfigParam("thread", thread.getText());
        start.setText("Остановить");
    }

    private boolean checkThreads() {
        if (Config.noProxy || tm.size() > 0) {
            return true;
        }
        int th = (Integer) threads.getValue();
        if (th > tm.getProxyManager().size() && !Config.noProxy) {
            JOptionPane.showMessageDialog(this, "Потоков не может быть больше чем проксей", "Ханю", JOptionPane.ERROR_MESSAGE);
            threads.setValue(tm.getProxyManager().size());
            return false;
        }
        if (th < 1) {
            JOptionPane.showMessageDialog(this, "Потоков не может быть ноль или отрицательное число", "Ханю", JOptionPane.ERROR_MESSAGE);
            threads.setValue(tm.getProxyManager().size());
            return false;
        }
        return true;
    }

    private void stop() {
        runThreads.setText("");
        ctxt.setText("");
        delSelected.setEnabled(false);
        stopSelected.setEnabled(false);
        suc.setText("Успешно:  0");
        suc.setVisible(false);
        failed.setText("Неудачно: 0");
        failed.setVisible(false);
        start.setText("Запустить");
    }

    private void setProxyState(Boolean b) {
        proxys.setEnabled(b);
        check.setEnabled(b);
        checkAll.setEnabled(b);
        delete.setEnabled(b);
        checkOnLoad.setEnabled(b);
        checkContent.setEnabled(b);
        checkOnStart.setEnabled(b);
    }

    @Override
    public void reSelectChan() {
        chans.setSelectedIndex(tm.getChanManager().getIndexName(Config.chanName));
        setTitle("Ханюшечка, чан: " + Config.chanName + ", Доска: " + Config.board);
    }
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
    }//GEN-LAST:event_formWindowClosing
    private boolean checkPage() {
        if (((Integer) page.getValue()) < 0) {
            return true;
        }
        return false;
    }
    private void formWindowStateChanged(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowStateChanged

        if (evt.getOldState() == 0) {
            setVisible(false);
        }
    }//GEN-LAST:event_formWindowStateChanged

    private void pasteNoMsgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pasteNoMsgActionPerformed
        Config.pasteNoMsg = pasteNoMsg.isSelected();
        Config.saveConfigParam("pasteNoMsg", Config.pasteNoMsg);
}//GEN-LAST:event_pasteNoMsgActionPerformed

    private void dontEditPixelsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dontEditPixelsActionPerformed
        Config.dontEditPixels = dontEditPixels.isSelected();
        Config.saveConfigParam("dontEditPixels", Config.dontEditPixels);
        colorRnd.setEnabled(!Config.dontEditPixels);
        pixelEdit.setEnabled(!Config.dontEditPixels);
        colorRndRnd.setEnabled(!Config.dontEditPixels);
}//GEN-LAST:event_dontEditPixelsActionPerformed

    private void pasteOnPicActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pasteOnPicActionPerformed
        Config.pasteOnPic = pasteOnPic.isSelected();
        pasteNoMsg.setEnabled(Config.pasteOnPic);
        picsTxtR.setEnabled(Config.pasteOnPic);
        picsTxtG.setEnabled(Config.pasteOnPic);
        picsTxtB.setEnabled(Config.pasteOnPic);
        fontSize.setEnabled(Config.pasteOnPic);
        startX.setEnabled(Config.pasteOnPic);
        startY.setEnabled(Config.pasteOnPic);
        deltaY.setEnabled(Config.pasteOnPic);
        FontName.setEnabled(Config.pasteOnPic);
        Config.saveConfigParam("pasteOnPic", Config.pasteOnPic);
}//GEN-LAST:event_pasteOnPicActionPerformed

    private void pixelEditKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pixelEditKeyTyped

        int c = (Integer) pixelEdit.getValue();
        if (c < 1) {
            pixelEdit.setValue(1);
            Config.pixelEdit = 1;
            Config.saveConfigParam("pixelEdit", 1);
            return;
        } else {
            Config.pixelEdit = c;
            Config.saveConfigParam("pixelEdit", c);
        }
}//GEN-LAST:event_pixelEditKeyTyped

    private void pixelEditStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_pixelEditStateChanged

        int c = (Integer) pixelEdit.getValue();
        if (c < 1) {
            pixelEdit.setValue(1);
            Config.pixelEdit = 1;
            Config.saveConfigParam("pixelEdit", 1);
            return;
        } else {
            Config.pixelEdit = c;
            Config.saveConfigParam("pixelEdit", c);
        }
}//GEN-LAST:event_pixelEditStateChanged

    private void colorRndKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_colorRndKeyTyped
        Config.colorRnd = (Integer) colorRnd.getValue();
        Config.saveConfigParam("colorRnd", Config.colorRnd);
}//GEN-LAST:event_colorRndKeyTyped

    private void colorRndStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_colorRndStateChanged

        Config.colorRnd = (Integer) colorRnd.getValue();
        Config.saveConfigParam("colorRnd", Config.colorRnd);
}//GEN-LAST:event_colorRndStateChanged

    private void maxFileSizeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_maxFileSizeKeyTyped

        int c = (Integer) maxFileSize.getValue();
        if (c < 1) {
            maxFileSize.setValue(1);
            return;
        } else {
            Config.maxFileSize = c;
            Config.saveConfigParam("maxFileSize", c);
        }
}//GEN-LAST:event_maxFileSizeKeyTyped

    private void maxFileSizeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_maxFileSizeStateChanged

        int c = (Integer) maxFileSize.getValue();
        if (c < 1) {
            maxFileSize.setValue(1);
            return;
        } else {
            Config.maxFileSize = c;
            Config.saveConfigParam("maxFileSize", c);
        }
}//GEN-LAST:event_maxFileSizeStateChanged

    private void useTmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_useTmpActionPerformed

        Config.useTmp = useTmp.isSelected();
        Config.saveConfigParam("useTmp", Config.useTmp);
        randomBytes.setEnabled(Config.useTmp);
}//GEN-LAST:event_useTmpActionPerformed

    private void oneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_oneActionPerformed
        String p = path.getText();
        if (p.contains(".j") || p.contains(".p") || p.contains(".g")) {
            one.setSelected(true);

        } else {
            one.setSelected(false);
        }
        Config.onePic = one.isSelected();
        Config.saveConfigParam("onePic", Config.onePic);
}//GEN-LAST:event_oneActionPerformed

    private void oneStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_oneStateChanged
}//GEN-LAST:event_oneStateChanged

    private void dontEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dontEditActionPerformed

        Config.picsNotEdit = dontEdit.isSelected();
        setPicsEnable(!Config.picsNotEdit);
        Config.saveConfigParam("picsNotEdit", Config.picsNotEdit);
}//GEN-LAST:event_dontEditActionPerformed

    private void picsPuckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_picsPuckActionPerformed

        Config.picsPuck = picsPuck.isSelected();
        Config.saveConfigParam("picsPuck", Config.picsPuck);
}//GEN-LAST:event_picsPuckActionPerformed

    private void noFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noFileActionPerformed

        noFile();
}//GEN-LAST:event_noFileActionPerformed

    private void noFile() {
        Config.BokuNoFile = noFile.isSelected();
        Config.saveConfigParam("BokuNoFile", Config.BokuNoFile);
        selectFileOrFolder.setEnabled(!Config.BokuNoFile);
        path.setEnabled(!Config.BokuNoFile);
        dontEdit.setEnabled(!Config.BokuNoFile);
        picsPuck.setEnabled(!Config.BokuNoFile);
        picsCount.setEnabled(!Config.BokuNoFile);
        maxFileSize.setEnabled(!Config.BokuNoFile);
        setPicsEnable(!Config.BokuNoFile && !Config.picsNotEdit);
    }
    private void selectFileOrFolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectFileOrFolderActionPerformed

        chooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_AND_DIRECTORIES);
        chooser.setCurrentDirectory(new File("./pic/"));
        FileFilter filterJpg = new FileNameExtensionFilter("JPG", "jpg,");
        FileFilter filterPng = new FileNameExtensionFilter("PNG", "png");
        FileFilter filterGif = new FileNameExtensionFilter("GIF", "gif");
        chooser.addChoosableFileFilter(filterGif);
        chooser.addChoosableFileFilter(filterJpg);
        chooser.addChoosableFileFilter(filterPng);
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File f = new File(chooser.getSelectedFile().getPath());
            path.setText(f.getAbsolutePath());
            if (f.isDirectory()) {
                picsCount.setText("Всего пикчей в паке: " + f.listFiles().length);
            }
            Config.path = f.getAbsolutePath();
            tm.reBuildImgManager();
            Config.saveConfigParam("path", Config.path);
        }
    }//GEN-LAST:event_selectFileOrFolderActionPerformed

    private void pathKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pathKeyPressed
    }//GEN-LAST:event_pathKeyPressed

    private void pathMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pathMouseClicked
    }//GEN-LAST:event_pathMouseClicked

    private void rndCountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rndCountKeyTyped

        int c = (Integer) rndCount.getValue();
        if (c < 1) {
            rndCount.setValue(1);
            Config.rndCount = 1;
            Config.saveConfigParam("rndCount", 1);
            return;
        } else {
            Config.rndCount = c;
            Config.saveConfigParam("rndCount", c);
        }
    }//GEN-LAST:event_rndCountKeyTyped

    public void addOCR(OCR o)
    {
        ocrs.addItem(o);
        logInfo("Added OCR: "+o.getInfo());
    }
    private void rndCountStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_rndCountStateChanged
        int c = (Integer) rndCount.getValue();
        if (c < 1) {
            rndCount.setValue(1);
            return;
        } else {
            Config.rndCount = c;
            Config.saveConfigParam("rndCount", c);
        }
    }//GEN-LAST:event_rndCountStateChanged

    @SuppressWarnings("CallToThreadDumpStack")
    private void fromfileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fromfileActionPerformed

        chooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);
        chooser.setCurrentDirectory(new File("./ini/"));
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            try {
                tm.getCopyPasteManager().load(chooser.getSelectedFile().getPath());
            } catch (Exception e) {
                e.printStackTrace();
            }
            all.setText("Всего копипасты: " + tm.getCopyPasteManager().size());
        }
}//GEN-LAST:event_fromfileActionPerformed

    private void randomizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_randomizeActionPerformed

        Config.randomizePaste = randomize.isSelected();
        rndCount.setEnabled(Config.randomizePaste);
        Config.saveConfigParam("randomizePaste", Config.randomizePaste);
}//GEN-LAST:event_randomizeActionPerformed

    private void symbolFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_symbolFocusLost

        Config.separator = symbol.getText();
        Config.saveConfigParam("separator", Config.separator);
}//GEN-LAST:event_symbolFocusLost

    private void symbolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_symbolActionPerformed
        // TODO add your handling code here:
}//GEN-LAST:event_symbolActionPerformed

    private void myseparatorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_myseparatorMouseClicked

        empty.setSelected(false);
        Config.emptySeparator = empty.isSelected();
        symbol.setEnabled(!Config.emptySeparator);
        Config.saveConfigParam("emptySeparator", Config.emptySeparator);
}//GEN-LAST:event_myseparatorMouseClicked

    private void emptyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_emptyMouseClicked

        Config.emptySeparator = empty.isSelected();
        myseparator.setSelected(!Config.emptySeparator);
        symbol.setEnabled(!Config.emptySeparator);
        Config.saveConfigParam("emptySeparator", Config.emptySeparator);
}//GEN-LAST:event_emptyMouseClicked

    @SuppressWarnings("static-access")
    private void reloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reloadActionPerformed

        tm.getCopyPasteManager().load(tm.getCopyPasteManager().CopyPastePath);
        all.setText("Всего копипасты: " + tm.getCopyPasteManager().size());
}//GEN-LAST:event_reloadActionPerformed

    @SuppressWarnings("CallToThreadDumpStack")
    private void loadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadActionPerformed

        chooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);
        chooser.setCurrentDirectory(new File("./ini/"));
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            try {
                tm.getProxyManager().load(chooser.getSelectedFile().getPath());
            } catch (Exception e) {
                e.printStackTrace();
            }
            pdlm.clear();
            for (HttpProxy p : tm.getProxyManager()) {
                addProxy(p);
            }
        }
}//GEN-LAST:event_loadActionPerformed

    private void noproxyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noproxyActionPerformed
        setProxyState(!noproxy.isSelected());
        Config.noProxy = noproxy.isSelected();
        Config.saveConfigParam("noProxy", Config.noProxy);
}//GEN-LAST:event_noproxyActionPerformed

    private void checkContentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkContentActionPerformed
        Config.contentCheck = checkContent.isSelected();
        syte.setEnabled(Config.contentCheck);
        syteKey.setEnabled(Config.contentCheck);
        Config.saveConfigParam("contentCheck", Config.contentCheck);
}//GEN-LAST:event_checkContentActionPerformed

    private void checkOnLoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkOnLoadActionPerformed

        Config.checkOnLoad = checkOnLoad.isSelected();
        Config.saveConfigParam("checkOnLoad", Config.checkOnLoad);
}//GEN-LAST:event_checkOnLoadActionPerformed

    private void deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteActionPerformed
        int i = JOptionPane.showConfirmDialog(this, "Точно удалить?", "Ханюша", JOptionPane.YES_NO_OPTION);
        if (i == JOptionPane.OK_OPTION) {
            for (Object o : proxys.getSelectedValues()) {
                HttpProxy p = (HttpProxy) o;
                pdlm.removeElement(p);
                tm.getProxyManager().remove(p);
                tm.getProxyManager().saveFile();
            }
        }
}//GEN-LAST:event_deleteActionPerformed

    public void removeProxy(HttpProxy p)
    {
        pdlm.removeElement(p);
    }
    private void checkAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkAllActionPerformed
        final UI ui = this;
        proxyProgress.setMaximum(tm.getProxyManager().size());
        tm.getProxyManager().progress=1;
        proxyCheck(proxys.getSelectedValues());
}//GEN-LAST:event_checkAllActionPerformed

    public void setProxyProgress(int i)
    {
        proxyProgress.setValue(i);
        proxyProgress.setString("Проверено "+i+" из "+proxyProgress.getMaximum());
        proxyProgress.setStringPainted(true);
    }

    private void checkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkActionPerformed
        proxyProgress.setMaximum(proxys.getSelectedValues().length);
        proxyCheck(proxys.getSelectedValues());
}//GEN-LAST:event_checkActionPerformed

    private void proxyCheck(final Object[] cproxys)
    {
        final UI ui=this;
        proxyProgress.setValue(0);    
        tm.getProxyManager().progress=1;
        Utils.startThread((new Runnable() {
            public void run() {
                for (Object o : cproxys) {
                     tm.getProxyManager().check((HttpProxy)o, ui);
                    /*if (pdlm.contains(o)) {
                        if (Config.contentCheck) {
                            showMessage("Ответ от " + Config.syte + " получен.", 1);
                        } else {
                            showMessage("Соединение с " + p.toString() + " успешно.", 1);
                        }
                    } else {
                        showMessage("Прокси " + p.toString() + " не работает..", 2);
                    }*/
                }
            }
        }));
    }
    private void ocrsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ocrsItemStateChanged
        setOCR();
}//GEN-LAST:event_ocrsItemStateChanged

    private void keyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_keyKeyTyped
    }//GEN-LAST:event_keyKeyTyped

    private void keyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_keyFocusLost

      Config.keyForServices = key.getText();
      Config.saveConfigParam("keyForServices", Config.keyForServices);


}//GEN-LAST:event_keyFocusLost

    private void keyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_keyActionPerformed
    }//GEN-LAST:event_keyActionPerformed

    private void keyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_keyMouseClicked
    }//GEN-LAST:event_keyMouseClicked

    private void msgCountMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_msgCountMouseClicked
        Config.msgCount = msgCount.isSelected();
        Config.saveConfigParam("msgCount", Config.msgCount);
        msgCountInteger.setEnabled(Config.msgCount);
}//GEN-LAST:event_msgCountMouseClicked

    private void msgCountIntegerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_msgCountIntegerKeyPressed

        countmessage();
}//GEN-LAST:event_msgCountIntegerKeyPressed

    private void msgCountIntegerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_msgCountIntegerStateChanged

        countmessage();
}//GEN-LAST:event_msgCountIntegerStateChanged

    public void updateThreadStatus() {
        runThreads.setText("[Поток: " + selectedWipe.toString() + "] [успешных: " + selectedWipe.getSuccessful() + "] [неудачных: " + selectedWipe.getFailed() + "][" + (wt.indexOf(selectedWipe) + 1) + " из " + wt.size()+" потоков]");
    }

    public void updateThreadStatus(AbstractWipe w) {
        runThreads.setText("[Поток: " + w.toString() + "] [успешных: " + w.getSuccessful() + "] [неудачных: " + w.getFailed() + "] [" + (wt.indexOf(w) + 1) + " из " + wt.size()+" потоков]");
    }
    private void silentBumpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_silentBumpActionPerformed
        Config.silentBump = silentBump.isSelected();
        Config.saveConfigParam("silentBump", Config.silentBump);
}//GEN-LAST:event_silentBumpActionPerformed

    private void boardFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_boardFocusLost

        Config.board = board.getText();
        Config.saveConfigParam("board", Config.board);
        setTitle("Ханюшечка. Чан: " + Config.chanName + ", Доска: " + Config.board);
}//GEN-LAST:event_boardFocusLost

    private void pageKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pageKeyPressed

        if (checkPage()) {
            page.setValue(0);
        }
        Config.page = (Integer) page.getValue();
        Config.saveConfigParam("page", Config.page);
}//GEN-LAST:event_pageKeyPressed

    private void pageStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_pageStateChanged

        if (checkPage()) {
            page.setValue(0);
        }
        Config.page = (Integer) page.getValue();
        Config.saveConfigParam("page", Config.page);
}//GEN-LAST:event_pageStateChanged

    private void chansActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chansActionPerformed
        if (!isVisible()) {
            return;
        }
        Config.chanName = (String) chans.getSelectedItem();
        Config.saveConfigParam("chanName", Config.chanName);
        setTitle("Ханюшечка. Чан: " + Config.chanName + ", Доска: " + Config.board);
        tm.getChanManager().createAndLoad();
        myInit();
}//GEN-LAST:event_chansActionPerformed

    private void reAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reAllActionPerformed
        for (AbstractWipe wp : wt)
            updateCaptcha(wp);
}//GEN-LAST:event_reAllActionPerformed

    private void sendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendActionPerformed
        send();
}//GEN-LAST:event_sendActionPerformed

    private void reMeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reMeActionPerformed
        if (send.isEnabled()) {
            ctxt.setText("");
            updateCaptcha(selectedWipe);
        }
    }//GEN-LAST:event_reMeActionPerformed

    private void setNextCaptcha()
    {
        Utils.startThread((new Runnable() {

            @SuppressWarnings("CallToThreadDumpStack")
            public void run() {
                captchas.setSelectedItem(getNextCaptcha(false));
            }
        }));
    }
    private void setFormedCaptcha()
    {
        Utils.startThread((new Runnable() {

            @SuppressWarnings("CallToThreadDumpStack")
            public void run() {
                captchas.setSelectedItem(getFormedCaptcha());
            }
        }));
    }
    private void ctxtKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ctxtKeyPressed

        switch (evt.getKeyCode()) {
            case java.awt.event.KeyEvent.VK_ENTER:
                send();
                break;
            case java.awt.event.KeyEvent.VK_F5:
                ctxt.setText("");
                updateCaptcha(selectedWipe);
                break;
            case java.awt.event.KeyEvent.VK_RIGHT:
                if (wt.size() == 1) {
                    break;
                }
                setNextCaptcha();
                break;
            case java.awt.event.KeyEvent.VK_LEFT:
                if (wt.size() == 1) {
                    break;
                }
                setFormedCaptcha();
                break;
            default:
                return;
                /*AbstractWipe ww = selectedWipe;
                ww.setCaptcha(ctxt.getText());
                ww = null;*/
        }
}//GEN-LAST:event_ctxtKeyPressed

    private void cimgMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cimgMouseEntered

        if (cimg.getIcon() != null && send.isEnabled()) {
            cimg.setCursor(handCursor);
        } else 
            cimg.setCursor(simpleCursor);
}//GEN-LAST:event_cimgMouseEntered

    private void cimgMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cimgMouseClicked

        if (send.isEnabled()) 
            updateCaptcha(selectedWipe);
}//GEN-LAST:event_cimgMouseClicked

    private void captchasItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_captchasItemStateChanged
    
        if (re) {
            re = false;
            return;
        }
        Utils.startThread((new Runnable() {

            public void run() {

                reMe.setEnabled(true);
                send.setEnabled(true);
                ctxt.setEnabled(true);
                ctxt.setText("");
                cimg.setText("");
                AbstractWipe ww;
                if (captchas.getSelectedItem() == null&&!wt.isEmpty()) {
                    ww = wt.get(0);
                } else {
                    ww = (AbstractWipe) captchas.getSelectedItem();
                }
                if(ww==null)
                    return;
                if (ww.getCaptcha()!=null&&ww.getCaptcha().isEmpty()) {
                    ctxt.setText(ww.getCaptcha());
                }
                selectedWipe = ww;
                if (!selectedWipe.isWork()) {
                    return;
                }
                if (ww.getCaptchaImg() == null) {
                    setCaptcha(ww);
                } else {
                    cimg.setIcon(ww.getCaptchaImg());

                }
                runThreads.setText("[Поток: " + ww.toString() + "] [успешных: " + ww.getSuccessful() + "] [неудачных: " + ww.getFailed() + "] [" + (wt.indexOf(ww) + 1) + " из " + wt.size()+" потоков ]");
                cimg.setToolTipText(ww.getProxy().getHost());
                status.setText(ww.getStatus());
                threads.setToolTipText(ww.getProxy().getHost());
                ww = null;
            }
        }));
        
}//GEN-LAST:event_captchasItemStateChanged

    private void hanyuuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hanyuuMouseClicked

        setVisible(false);
        Faggot.getinstance().display();
}//GEN-LAST:event_hanyuuMouseClicked

    private void startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startActionPerformed

        Config.msg = message.getText();
        Config.saveConfigParam("msg", Config.msg);
        Config.thread = thread.getText();
        Config.saveConfigParam("thread", Config.thread);
        if (!checkThreads()) {
            return;
        }

        Utils.startThread((new Runnable() {
            @SuppressWarnings("CallToThreadDumpStack")
            public void run() {
                tm.StartStopManage();
            }
        }));
}//GEN-LAST:event_startActionPerformed

    private boolean checkRGBReange(int rgb, javax.swing.JSpinner js) {
        boolean r = -1 < rgb && rgb < 255;
        if (!r) {
            js.setValue(0);
        }
        return r;
    }
    private void picsTxtRStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_picsTxtRStateChanged
        int rgb = (Integer) picsTxtR.getValue();
        if (!checkRGBReange(rgb, picsTxtR)) {
            rgb = 0;
        }
        Config.picsTxtR = rgb;
        Config.saveConfigParam("picsTxtR", Config.picsTxtR);
    }//GEN-LAST:event_picsTxtRStateChanged

    private void picsTxtRKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_picsTxtRKeyTyped
        int rgb = (Integer) picsTxtR.getValue();
        if (!checkRGBReange(rgb, picsTxtR)) {
            rgb = 0;
        }
        Config.picsTxtR = rgb;
        Config.saveConfigParam("picsTxtR", Config.picsTxtR);
    }//GEN-LAST:event_picsTxtRKeyTyped

    public void seWindowtState(int state) {
        setState(state);
    }
    private void picsTxtGStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_picsTxtGStateChanged
        int rgb = (Integer) picsTxtG.getValue();
        if (!checkRGBReange(rgb, picsTxtG)) {
            rgb = 0;
        }
        Config.picsTxtG = rgb;
        Config.saveConfigParam("picsTxtG", Config.picsTxtG);
    }//GEN-LAST:event_picsTxtGStateChanged

    private void picsTxtGKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_picsTxtGKeyTyped
        int rgb = (Integer) picsTxtG.getValue();
        if (!checkRGBReange(rgb, picsTxtG)) {
            rgb = 0;
        }
        Config.picsTxtG = rgb;
        Config.saveConfigParam("picsTxtG", Config.picsTxtG);
    }//GEN-LAST:event_picsTxtGKeyTyped

    private void picsTxtBStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_picsTxtBStateChanged
        int rgb = (Integer) picsTxtB.getValue();
        if (!checkRGBReange(rgb, picsTxtB)) {
            rgb = 0;
        }
        Config.picsTxtB = rgb;
        Config.saveConfigParam("picsTxtB", Config.picsTxtB);
    }//GEN-LAST:event_picsTxtBStateChanged

    private void picsTxtBKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_picsTxtBKeyTyped
        int rgb = (Integer) picsTxtB.getValue();
        if (!checkRGBReange(rgb, picsTxtB)) {
            rgb = 0;
        }
        Config.picsTxtB = rgb;
        Config.saveConfigParam("picsTxtB", Config.picsTxtB);
    }//GEN-LAST:event_picsTxtBKeyTyped

    private void startXKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_startXKeyTyped
        int x = (Integer) startX.getValue();
        if (x < 0) {
            x = 0;
        }
        startX.setValue(x);
        Config.startX = x;
        Config.saveConfigParam("startX", x);
    }//GEN-LAST:event_startXKeyTyped

    private void startXStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_startXStateChanged
        int x = (Integer) startX.getValue();
        if (x < 0) {
            x = 0;
        }
        startX.setValue(x);
        Config.startX = x;
        Config.saveConfigParam("startX", x);
    }//GEN-LAST:event_startXStateChanged

    private void startYStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_startYStateChanged
        int y = (Integer) startY.getValue();
        if (y < 0) {
            y = 0;
        }
        startY.setValue(y);
        Config.startY = y;
        Config.saveConfigParam("startY", y);
    }//GEN-LAST:event_startYStateChanged

    private void startYKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_startYKeyTyped
        int y = (Integer) startY.getValue();
        if (y < 0) {
            y = 0;
        }
        startY.setValue(y);
        Config.startY = y;
        Config.saveConfigParam("startY", y);
    }//GEN-LAST:event_startYKeyTyped

    private void deltaYKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_deltaYKeyTyped
        int y = (Integer) deltaY.getValue();
        if (y < 0) {
            y = 0;
        }
        deltaY.setValue(y);
        Config.deltaY = y;
        Config.saveConfigParam("deltaY", y);
    }//GEN-LAST:event_deltaYKeyTyped

    private void deltaYStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_deltaYStateChanged
        int y = (Integer) deltaY.getValue();
        if (y < 0) {
            y = 0;
        }
        deltaY.setValue(y);
        Config.deltaY = y;
        Config.saveConfigParam("deltaY", y);
    }//GEN-LAST:event_deltaYStateChanged

    
    private void FontNameKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_FontNameKeyTyped
        Config.FontName = FontName.getText();
        Config.saveConfigParam("FontName", Config.FontName);
    }//GEN-LAST:event_FontNameKeyTyped

    private void FontNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_FontNameFocusLost
        Config.FontName = FontName.getText();
        Config.saveConfigParam("FontName", Config.FontName);
    }//GEN-LAST:event_FontNameFocusLost

    private void fontSizeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_fontSizeStateChanged
        int s = (Integer) fontSize.getValue();
        if (s < 1) {
            s = 1;
        }
        fontSize.setValue(s);
        Config.fontSize = s;
        Config.saveConfigParam("fontSize", s);
    }//GEN-LAST:event_fontSizeStateChanged

    private void fontSizeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fontSizeKeyTyped
        int s = (Integer) fontSize.getValue();
        if (s < 1) {
            s = 1;
        }
        fontSize.setValue(s);
        Config.fontSize = s;
        Config.saveConfigParam("fontSize", s);
    }//GEN-LAST:event_fontSizeKeyTyped

    private void colorRndRndActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_colorRndRndActionPerformed
        Config.colorRndRnd = colorRndRnd.isSelected();
        Config.saveConfigParam("colorRndRnd", Config.colorRndRnd);
        colorRnd.setEnabled(!Config.colorRndRnd);
    }//GEN-LAST:event_colorRndRndActionPerformed

    private void ontopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ontopActionPerformed
        setAlwaysOnTop(ontop.isSelected());
    }//GEN-LAST:event_ontopActionPerformed

    private void formFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_formFocusGained
        ctxt.requestFocus();
    }//GEN-LAST:event_formFocusGained

    private void mainMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mainMouseClicked
        ctxt.requestFocus();
    }//GEN-LAST:event_mainMouseClicked

    private void checkOnStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkOnStartActionPerformed

        Config.emulateWipe = checkOnStart.isSelected();
        Config.saveConfigParam("emulateWipe", Config.emulateWipe);

    }//GEN-LAST:event_checkOnStartActionPerformed

    private void threadsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_threadsKeyPressed
        if (checkThreads()) {
            Config.threads = (Integer) threads.getValue();
            Config.saveConfigParam("threads", Config.threads);
        }
    }//GEN-LAST:event_threadsKeyPressed

    private void threadsStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_threadsStateChanged
        if (checkThreads()) {
            Config.threads = (Integer) threads.getValue();
            Config.saveConfigParam("threads", Config.threads);
        }
    }//GEN-LAST:event_threadsStateChanged

    private void timeoutStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_timeoutStateChanged
        int t = (Integer) timeout.getValue();
        if (t < 0) {
            timeout.setValue(0);
        }
        t = (Integer) timeout.getValue();
        Config.timeout = t;
        Config.saveConfigParam("timeOut", t);
    }//GEN-LAST:event_timeoutStateChanged

    private void timeoutKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_timeoutKeyPressed

        int t = (Integer) timeout.getValue();
        if (t < 0) {
            timeout.setValue(0);
        }
        t = (Integer) timeout.getValue();
        Config.timeout = t;
        Config.saveConfigParam("timeOut", t);
    }//GEN-LAST:event_timeoutKeyPressed

    private void mainFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_mainFocusLost
    }//GEN-LAST:event_mainFocusLost

    private void nameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nameFocusLost

        Config.name = name.getText();
        Config.saveConfigParam("name", Config.name);
    }//GEN-LAST:event_nameFocusLost

    private void themeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_themeFocusLost

        Config.theme = theme.getText();

        Config.saveConfigParam("theme", Config.theme);
    }//GEN-LAST:event_themeFocusLost

    private void userAgentFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_userAgentFocusLost

        Config.userAgent = userAgent.getText();
        Config.saveConfigParam("userAgent", Config.userAgent);
    }//GEN-LAST:event_userAgentFocusLost

    private void passwordFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_passwordFocusLost

        Config.password = password.getText();
        Config.saveConfigParam("password", Config.password);
    }//GEN-LAST:event_passwordFocusLost

    private void randomBytesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_randomBytesActionPerformed
        Config.randomBytes = randomBytes.isSelected();
        Config.saveConfigParam("randomBytes", Config.randomBytes);
    }//GEN-LAST:event_randomBytesActionPerformed

    private void syteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_syteFocusLost
        syte();
    }//GEN-LAST:event_syteFocusLost

    private void syteKeyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_syteKeyFocusLost

        key();
    }//GEN-LAST:event_syteKeyFocusLost

    private void key() {
        if (syteKey.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Не указан ожидаемый ответ от сайта " + Config.syte, "Ханюша", JOptionPane.ERROR_MESSAGE);
            mainPane.setSelectedIndex(2);
            return;
        }

        Config.syteKey = syteKey.getText();
        Config.saveConfigParam("syteKey", Config.syteKey);
    }

    private void syte() {
        if (syte.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Не указан сайт.", "Ханюша", JOptionPane.ERROR_MESSAGE);
            mainPane.setSelectedIndex(2);
            return;
        }
        Config.syte = syte.getText();
        Config.saveConfigParam("syte", Config.syte);
    }
    private void syteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_syteKeyTyped
        syte();
    }//GEN-LAST:event_syteKeyTyped

    private void syteKeyKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_syteKeyKeyTyped
        key();
    }//GEN-LAST:event_syteKeyKeyTyped

    private void dontTrayMsgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dontTrayMsgActionPerformed
        Config.dontTrayMsg = dontTrayMsg.isSelected();
        Config.saveConfigParam("dontTrayMsg", Config.dontTrayMsg);
    }//GEN-LAST:event_dontTrayMsgActionPerformed

    private void nextCaptchaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextCaptchaActionPerformed
        captchas.setSelectedItem((getNextCaptcha(false)));
    }//GEN-LAST:event_nextCaptchaActionPerformed

    private void formerCaptchaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_formerCaptchaActionPerformed
        captchas.setSelectedItem(getFormedCaptcha());
    }//GEN-LAST:event_formerCaptchaActionPerformed

    private void delSelectedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delSelectedActionPerformed
        final UI ui = this;
        Utils.startThread((new Runnable() {

            public void run() {
                selectedWipe.destroy("Удалено пользователем.");
                removeThread(null);
                captchas.setSelectedItem(getNextCaptcha(true));
                updateThreadStatus();
                tm.getProxyManager().delete(selectedWipe.getProxy(), ui);
            }
        }));
        
    }//GEN-LAST:event_delSelectedActionPerformed

    private void stopSelectedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopSelectedActionPerformed

        Utils.startThread((new Runnable() {

            public void run() {
                selectedWipe.destroy("Остановлено пользователем.");
                removeThread(null);
                captchas.setSelectedItem(getNextCaptcha(true));
                updateThreadStatus();
            }
        }));
        
    }//GEN-LAST:event_stopSelectedActionPerformed

    private void threadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_threadKeyPressed
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            Config.thread = thread.getText();
            Config.saveConfigParam("thread", Config.thread);
        }
    }//GEN-LAST:event_threadKeyPressed

private void AddRrowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddRrowActionPerformed

    dtm.setRowCount(dtm.getRowCount() + 1);
}//GEN-LAST:event_AddRrowActionPerformed

    private void RemoveRrowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RemoveRrowActionPerformed
        dtm.removeRow(replacements.getSelectedRow());
        sr();
        

    }//GEN-LAST:event_RemoveRrowActionPerformed

    private void sr() {
        Config.tr = "";
        if (dtm.getRowCount() > 0) {
            for (int i = 0; i < dtm.getRowCount(); i++) {
                Config.tr += dtm.getValueAt(i, 0).toString() + ":" + dtm.getValueAt(i, 1).toString() + ";";
            }
        }

        Config.saveConfigParam("replacements", Config.tr);
        Config.buildReplacements();
    }
private void saverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saverActionPerformed

    sr();
}//GEN-LAST:event_saverActionPerformed

private void randomPicGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_randomPicGenerateActionPerformed
    
    Config.randomPicGenerate = randomPicGenerate.isSelected();
    Config.saveConfigParam("randomPicGenerate", Config.randomPicGenerate);
    Config.useTmp = !Config.randomPicGenerate;
    Config.saveConfigParam("useTmp", Config.useTmp);
    useTmp.setSelected(Config.useTmp);
}//GEN-LAST:event_randomPicGenerateActionPerformed

private void shutdownActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_shutdownActionPerformed
    
    int index=scriptsList.getSelectedIndex();
    
    ScriptContainer s=(ScriptContainer)sdlm.get(index);
    sdlm.remove(index);
    tm.getScripts().getScripts().remove(s);
}//GEN-LAST:event_shutdownActionPerformed

private void reLoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reLoadActionPerformed

    int index=scriptsList.getSelectedIndex();
    
    ScriptContainer s=(ScriptContainer)sdlm.get(index);
    sdlm.remove(index);
    tm.getScripts().reLoad(s);
    
}//GEN-LAST:event_reLoadActionPerformed

private void scriptsListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_scriptsListMouseClicked

    int index=scriptsList.getSelectedIndex();
    if(index>-1)
    {
        ScriptContainer s=(ScriptContainer)sdlm.get(index);
        dontUseScript.setSelected(!s.use);
    }
}//GEN-LAST:event_scriptsListMouseClicked

private void dontUseScriptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dontUseScriptActionPerformed
int index=scriptsList.getSelectedIndex();
    if(index>-1)
    {
        ScriptContainer s=(ScriptContainer)sdlm.get(index);
        s.use=!dontUseScript.isSelected();
    }
}//GEN-LAST:event_dontUseScriptActionPerformed

private void loadScriptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadScriptActionPerformed

    chooser.setFileSelectionMode(javax.swing.JFileChooser.FILES_ONLY);
    chooser.setCurrentDirectory(new File("./scripts/"));
    int returnVal = chooser.showOpenDialog(this);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        tm.getScripts().load(chooser.getSelectedFile());
            
    }
}//GEN-LAST:event_loadScriptActionPerformed

private void modesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_modesItemStateChanged
    switch(modes.getSelectedIndex())
    {
        case 0:
            Config.workMode=WorkMode.WipeBoard;
            page.setEnabled(false);
            thread.setEnabled(false);
            break;
        case 1:
            Config.workMode=WorkMode.Force;
            page.setEnabled(true);
            thread.setEnabled(false);
            break;
        case 2:
            Config.workMode=WorkMode.OnZeroPage;
            page.setEnabled(false);
            thread.setEnabled(true);
            break;
        case 3:
            Config.workMode=WorkMode.WipeThread;
            page.setEnabled(false);
            thread.setEnabled(true);
            break;
      
    }
    
    Config.saveConfigParam("workMode", Config.workMode.toString());
}//GEN-LAST:event_modesItemStateChanged

private void useParserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_useParserActionPerformed
    Config.parser=useParser.isSelected();
    Config.saveConfigParam("parser", Config.parser);
    silentBump.setEnabled(Config.parser);
    dellAll.setEnabled(Config.parser);
}//GEN-LAST:event_useParserActionPerformed

private void scaleWFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_scaleWFocusLost

    Config.scaleW=Double.valueOf(scaleW.getText());
    Config.saveConfigParam("scaleW", Config.scaleW);
}//GEN-LAST:event_scaleWFocusLost

private void scaleHFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_scaleHFocusLost

    Config.scaleH=Double.valueOf(scaleH.getText());
    Config.saveConfigParam("scaleH", Config.scaleH);
}//GEN-LAST:event_scaleHFocusLost

private void dellAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dellAllActionPerformed
    tm.deleteAllMessages();
}//GEN-LAST:event_dellAllActionPerformed

private void reverseCaptchaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reverseCaptchaActionPerformed
   Config.reverseCaptcha=reverseCaptcha.isSelected();
   Config.saveConfigParam("reverseCaptcha", Config.reverseCaptcha);
}//GEN-LAST:event_reverseCaptchaActionPerformed

private void emailFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_emailFocusLost
    Config.email=email.getText();
    Config.saveConfigParam("email", Config.email);
}//GEN-LAST:event_emailFocusLost

private void smartErrorHandlerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_smartErrorHandlerActionPerformed
    Config.smartErrorHandler=smartErrorHandler.isSelected();
    Config.saveConfigParam("smartErrorHandler", Config.smartErrorHandler);
    smartErrorCount.setEnabled(Config.smartErrorHandler);
    smartErrorAction.setEnabled(Config.smartErrorHandler);
}//GEN-LAST:event_smartErrorHandlerActionPerformed

private void smartErrorCountFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_smartErrorCountFocusLost
    int errors;
    try
    {
        errors=Integer.parseInt(smartErrorCount.getText());
    }
    catch(Exception e)
    {
        this.showMessage("Вы ввели не число.", 0);
        return;
    }
    Config.smartErrorCount=errors;
    Config.saveConfigParam("smartErrorCount", Config.smartErrorCount);
}//GEN-LAST:event_smartErrorCountFocusLost

private void smartErrorActionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_smartErrorActionItemStateChanged
    Config.smartErrorAction=smartErrorAction.getSelectedIndex();
    Config.saveConfigParam("smartErrorAction", Config.smartErrorAction);
}//GEN-LAST:event_smartErrorActionItemStateChanged

private void waitForNotReadyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_waitForNotReadyActionPerformed

    Config.waitForNotReady=waitForNotReady.isSelected();
    Config.saveConfigParam("waitForNotReady", Config.waitForNotReady);
}//GEN-LAST:event_waitForNotReadyActionPerformed

private void messageFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_messageFocusLost
    Config.msg=message.getText();
    Config.saveConfigParam("msg", Config.msg);
}//GEN-LAST:event_messageFocusLost

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
   tm.runThreads();
}//GEN-LAST:event_jButton1ActionPerformed
    
    public void addScript(ScriptContainer s)
    {
        sdlm.addElement(s);
        logInfo("Added script: "+s.script.getInfo());
    }
    private void updateCaptcha(final AbstractWipe ww) {

        Utils.startThread(new Runnable() {

            @SuppressWarnings("CallToThreadDumpStack")
            public void run() {
                try {
                    cimg.setIcon(null);
                    cimg.setText("Обновляем...");
                    InputStream in = CaptchaUtils.getScaledCaptchaStream(Config.scaleW, Config.scaleH,ww);
                    cimg.setText("");
                    captchaImage = new ImageIcon(ImageIO.read(in));
                    
                    in.close();
                    if (selectedWipe == ww) {
                        cimg.setIcon(captchaImage);
                    } else {
                        ww.setCaptchaImg(captchaImage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void send() {
        if (ctxt.getText().isEmpty() || cimg.getIcon() == null) {
            return;
        }
        Utils.startThread((new Runnable() {

            public void run() {
                AbstractWipe ww = selectedWipe;
                setUpState(false);
                ww.setCaptchaImg(null);
                ww.setStatus("Посылаем..");
                setStatus(ww);
                //logInfo(ww.getProxy().getHost()+" "+ctxt.getText());
                ww.setCaptcha(ctxt.getText());
                ctxt.setText("");
                if (captchas.getItemCount() > 1) {
                    captchas.setSelectedItem(getNextCaptcha(true));
                }
                ww.myNotify();
                /*if (fromTray) {
                    setVisible(false);
                }*/
            }
        }));
    }

    public void setStatus(AbstractWipe ww) {
        if (selectedWipe == ww) {
            status.setText(ww.getStatus());
        }
    }

    private AbstractWipe getNextCaptcha(boolean check) {
        if (ctxt.getText().isEmpty() && check) {
            getNextCaptcha(false);
        }
        int m = wt.indexOf(selectedWipe) + 1;
        if (m == wt.size()) {
            return wt.get(0);
        } else {
            return wt.get(m++);
        }
    }

    private AbstractWipe getFormedCaptcha() {
        int m = wt.indexOf(selectedWipe);
        if (m == 0) {
            System.out.println(m + ":" + wt.size() + ":" + wt.indexOf(selectedWipe) + ":" + wt.get(wt.size() - 1));
            return wt.get(wt.size() - 1);
        } else {
            System.out.println(m + ":" + wt.size() + ":" + wt.indexOf(selectedWipe) + ":" + wt.get(m--));
            return wt.get(m--);
        }
    }

    private void setPicsEnable(boolean b) {
        one.setEnabled(b);
        useTmp.setEnabled(b);
        colorRnd.setEnabled(b && !Config.dontEditPixels && !Config.colorRndRnd);
        pixelEdit.setEnabled(b && !Config.dontEditPixels);
        colorRndRnd.setEnabled(b && !Config.dontEditPixels);
        pasteOnPic.setEnabled(b);
        randomBytes.setEnabled(b);
        pasteNoMsg.setEnabled(b && Config.pasteOnPic);
        pasteNoMsg.setEnabled(b && Config.pasteOnPic);
        picsTxtR.setEnabled(b && Config.pasteOnPic);
        picsTxtG.setEnabled(b && Config.pasteOnPic);
        picsTxtB.setEnabled(b && Config.pasteOnPic);
        dontEditPixels.setEnabled(b);
        startX.setEnabled(b && Config.pasteOnPic);
        startY.setEnabled(b && Config.pasteOnPic);
        deltaY.setEnabled(b && Config.pasteOnPic);
        fontSize.setEnabled(b && Config.pasteOnPic);
        FontName.setEnabled(b && Config.pasteOnPic);
    }

    private void countmessage() {
        int count = (Integer) msgCountInteger.getValue();
        if (count < 1) {
            msgCountInteger.setValue(1);
            return;
        } else {
            Config.msgCountInt = count;
            Config.saveConfigParam("msgCountInt", count);
        }

    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void setCaptcha(AbstractWipe ww) {
        try {
            cimg.setText("");
            if (!tm.isWork() || ww == null || !ww.isWork()) {
                return;
            }
            if (!isVisible()&&!dontJamp.isSelected()) {
                fromTray = true;
                setVisible(true);
                setState(0);
            }
            if (!wt.contains(ww)) {
                re = true;
                captchas.addItem(ww);
                wt.add(ww);
                selectedWipe = (AbstractWipe) captchas.getSelectedItem();
                if(selectedWipe==null)
                    return;
                if (wt.size() == tm.size()) {
                    captchas.setSelectedIndex(wt.indexOf(ww));
                    selectedWipe = (AbstractWipe) captchas.getSelectedItem();
                }
            }
            if (wt.size() > 1) {
                nextCaptcha.setEnabled(true);
                formerCaptcha.setEnabled(true);
            }
            runThreads.setText("[Поток: " + selectedWipe.toString() + "] [успешных: " + selectedWipe.getSuccessful() + "] [неудачных: " + selectedWipe.getFailed() + "] [" + (wt.indexOf(ww) + 1) + " из " + wt.size()+" потоков]");
            status.setText("");
            cimg.setText("");

            if (ww.getCaptchaImg() != null) {
                captchaImage = ww.getCaptchaImg();
                return;
            }
            if (selectedWipe == ww) {
                cimg.setIcon(null);
                cimg.setText("Обновляем капчу..");
            }
            InputStream in = CaptchaUtils.getScaledCaptchaStream(Config.scaleW, Config.scaleH,ww);

            if(in==null){
                ww.destroy("No InputCaptcha");
                cimg.setText("");
            }
            captchaImage = new ImageIcon(ImageIO.read(in));

            in.close();
            /*if(captchaImage==null)
                ww.destroy("No InputCaptcha");*/
            ww.setCaptchaImg(captchaImage);
            if (selectedWipe.getProxy().getHost().contains(ww.getProxy().getHost())) {
                
                cimg.setIcon(captchaImage);
                cimg.setText("");
                cimg.setToolTipText(ww.getProxy().getHost());               
            }
            setUpState(true);
        } catch (Exception e) {
            e.printStackTrace();
            cimg.setIcon(null);
            cimg.setText("");
            tm.getProxyManager().delete(ww.getProxy(), this);
            ww.destroy(e.toString());
        }

    }

    public void setUpState(boolean b) {
        if (!b) {
            cimg.setIcon(null);
        }
        reMe.setEnabled(b);
        send.setEnabled(b);
        reAll.setEnabled(b);
        ctxt.setEnabled(b);
        ctxt.requestFocus();
        status.setText("");
    }

    public void removeThread(AbstractWipe w) {

        captchas.removeItem(w);
        wt.remove(w);

        if (captchas.getItemCount() > 1) {
            captchas.setSelectedItem(getNextCaptcha(true));
        }


        nextCaptcha.setEnabled(!(wt.size() == 1));
        formerCaptcha.setEnabled(!(wt.size() == 1));

        if (wt.isEmpty()) {
            cimg.setIcon(null);
            send.setEnabled(false);
        }

    }

    public void addProxy(HttpProxy p) {
        pdlm.add(pdlm.size(), p);
    }

    private void setOCR()
    {
        if(!isVisible())
            return;
        Config.ocrMode=ocrs.getSelectedItem().toString();
        Config.saveConfigParam("ocrMode", Config.ocrMode);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddRrow;
    private javax.swing.JTextField FontName;
    private javax.swing.JButton RemoveRrow;
    private javax.swing.JLabel all;
    private javax.swing.JTextField board;
    private javax.swing.JComboBox captchas;
    private javax.swing.JComboBox chans;
    private javax.swing.JButton check;
    private javax.swing.JButton checkAll;
    private javax.swing.JCheckBox checkContent;
    private javax.swing.JCheckBox checkOnLoad;
    private javax.swing.JCheckBox checkOnStart;
    private javax.swing.JLabel cimg;
    private javax.swing.JSpinner colorRnd;
    private javax.swing.JCheckBox colorRndRnd;
    private javax.swing.JTextField ctxt;
    private javax.swing.JButton delSelected;
    private javax.swing.JButton delete;
    private javax.swing.JButton dellAll;
    private javax.swing.JSpinner deltaY;
    private javax.swing.JCheckBox dontEdit;
    private javax.swing.JCheckBox dontEditPixels;
    private javax.swing.JCheckBox dontJamp;
    private javax.swing.JCheckBox dontTrayMsg;
    private javax.swing.JCheckBox dontUseScript;
    private javax.swing.JTextField email;
    private javax.swing.JRadioButton empty;
    private javax.swing.JLabel failed;
    private javax.swing.JSpinner fontSize;
    private javax.swing.JButton formerCaptcha;
    private javax.swing.JButton fromfile;
    private javax.swing.JLabel hanyuu;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTextField key;
    private javax.swing.JButton load;
    private javax.swing.JButton loadScript;
    private javax.swing.JTextPane log;
    private javax.swing.JScrollPane logScroll;
    private javax.swing.JPanel main;
    private javax.swing.JTabbedPane mainPane;
    private javax.swing.JSpinner maxFileSize;
    private javax.swing.JTextPane message;
    private javax.swing.JComboBox modes;
    private javax.swing.JCheckBox msgCount;
    private javax.swing.JSpinner msgCountInteger;
    private javax.swing.JRadioButton myseparator;
    private javax.swing.JTextField name;
    private javax.swing.JButton nextCaptcha;
    private javax.swing.JCheckBox noFile;
    private javax.swing.JCheckBox noproxy;
    private javax.swing.JLabel ocrName;
    private javax.swing.JComboBox ocrs;
    private javax.swing.JCheckBox one;
    private javax.swing.JCheckBox ontop;
    private javax.swing.JSpinner page;
    private javax.swing.JTextField password;
    private javax.swing.JPanel paste;
    private javax.swing.JCheckBox pasteNoMsg;
    private javax.swing.JCheckBox pasteOnPic;
    private javax.swing.JTextField path;
    private javax.swing.JPanel pics;
    private javax.swing.JLabel picsCount;
    private javax.swing.JCheckBox picsPuck;
    private javax.swing.JSpinner picsTxtB;
    private javax.swing.JSpinner picsTxtG;
    private javax.swing.JSpinner picsTxtR;
    private javax.swing.JSpinner pixelEdit;
    private javax.swing.JPanel proxy;
    private javax.swing.JProgressBar proxyProgress;
    private javax.swing.JList proxys;
    private javax.swing.JCheckBox randomBytes;
    private javax.swing.JCheckBox randomPicGenerate;
    private javax.swing.JCheckBox randomize;
    private javax.swing.JButton reAll;
    private javax.swing.JButton reLoad;
    private javax.swing.JButton reMe;
    private javax.swing.JButton reload;
    private javax.swing.JTable replacements;
    private javax.swing.JCheckBox reverseCaptcha;
    private javax.swing.JSpinner rndCount;
    private javax.swing.JLabel runThreads;
    private javax.swing.JButton saver;
    private javax.swing.JTextField scaleH;
    private javax.swing.JTextField scaleW;
    private javax.swing.JLabel scriptInfo;
    private javax.swing.JPanel scripts;
    private javax.swing.JList scriptsList;
    private javax.swing.JButton selectFileOrFolder;
    private javax.swing.JButton send;
    private javax.swing.JPanel settings;
    private javax.swing.JButton shutdown;
    private javax.swing.JCheckBox silentBump;
    private javax.swing.JComboBox smartErrorAction;
    private javax.swing.JTextField smartErrorCount;
    private javax.swing.JCheckBox smartErrorHandler;
    private javax.swing.JButton start;
    private javax.swing.JSpinner startX;
    private javax.swing.JSpinner startY;
    private javax.swing.JLabel status;
    private javax.swing.JButton stopSelected;
    private javax.swing.JLabel suc;
    private javax.swing.JTextField symbol;
    private javax.swing.JTextField syte;
    private javax.swing.JTextField syteKey;
    private javax.swing.JTextField theme;
    private javax.swing.JTextField thread;
    private javax.swing.JSpinner threads;
    private javax.swing.JSpinner timeout;
    private javax.swing.JCheckBox useParser;
    private javax.swing.JCheckBox useTmp;
    private javax.swing.JTextField userAgent;
    private javax.swing.JCheckBox waitForNotReady;
    // End of variables declaration//GEN-END:variables
}
