/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ui.interfaces;
import hanyuu.ext.ScriptContainer;
import hanyuu.net.proxy.HttpProxy;
import hanyuu.net.wipe.AbstractWipe;
import hanyuu.ext.interfaces.OCR;
/**
 *
 * @author Hanyuu Furude
 */
public interface UI
{
    public void SwitchStartStop();
    public int  getThreads();
    public void logInfo(String s);
    public void logError(String s);
    public void setVisible(boolean b);
    public void setTitle(String s);
    public void addChan(String s);
    public void reSelectChan();
    public void setCaptcha(AbstractWipe w);
    public void updateThreadStatus();
    public void updateThreadStatus(AbstractWipe w);
    public abstract void removeThread(AbstractWipe par1);
    public void setStatus(AbstractWipe ww);
    public void setUpState(boolean b);
    public void setSuccessful(int i);
    public void setFailed(int i);
    public void seWindowtState(int i);
    public void addScript(ScriptContainer s);
    public void addProxy(HttpProxy p);
    public void removeProxy(HttpProxy p);
    public void setProxyProgress(int i);
    public void addOCR(OCR ocr);
    public void reSelectOCR();

    /**
    Used for error messages. <code>0</code><br/>
    Used for information messages. <code>1</code><br/>
    Used for warning messages. <code>2</code><br/>
    Used for questions. <code>3</code><br/>
    No icon is used. <code>-1</code>
    */
    public void showMessage(String msg,int type);
}
