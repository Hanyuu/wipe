/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ui.interfaces;

/**
 *
 * @author Hanyuu Furude
 */
public interface ITray
{
    public void setUI(UI ui);
    public void addIcon();
    public void trayPrint(String s,String ss);
    public void reIcon(int s,int f);
    public void trayPrintError(String s,String ss);
    public void switchState();
}
