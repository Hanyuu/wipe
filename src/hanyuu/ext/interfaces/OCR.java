/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hanyuu.ext.interfaces;
import hanyuu.net.wipe.AbstractWipe;
/**
 *
 * @author Hanyuu Furude
 */
public interface OCR extends SimpleScript
{
    public void recognizeCaptcha(AbstractWipe w);
    public void onError();
    public void onSuccess();
    @Override
    public String toString();
}
