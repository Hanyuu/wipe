/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.ext.interfaces;

import hanyuu.net.wipe.AbstractWipe;
import java.util.ArrayList;
import org.apache.http.HttpHost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.HttpResponse;

/**
 *
 * @author Hanyuu Furude
 */
public interface WipeExtension extends SimpleScript{

    /**Метод будет вызван при запуске потока*/
    public abstract void onRunWipe(AbstractWipe w);

    /**Метод будет вызван при остновке потока*/
    public abstract void onStopWipe(AbstractWipe w);

    /**Эти методы позволяют модифицировть данные от потока Wipe для Get запроса через скрипт, если не нужно то не изменяйте или верните null<br/>
    Страрайтесь что бы скрипты не запороли друг друга.
     */
    public Object[] onGetRequest(AbstractWipe w, String url, HttpHost host, ArrayList<String> headers, boolean saveCookies);

    /**Эти методы позволяют модифицировть данные от потока Wipe для Post запроса через скрипт, если не нужно то не изменяйте или верните null*/
    public Object[] onPostRequest(AbstractWipe w, String url, HttpHost host, MultipartEntity me, ArrayList<String> headers);

    /**Срабатывает после получения ответа на Get запрос и предоставляет ответ для обработки и/или модификации.*/
    public HttpResponse onGetAnswer(AbstractWipe w, HttpResponse hr);

    /**Срабатывает после получения ответа на Post запрос и предоставляет ответ для обработки и/или модификации.*/
    public HttpResponse onPostAnswer(AbstractWipe w, HttpResponse hr);
}
