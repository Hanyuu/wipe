/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hanyuu.ext.interfaces;

/**
 *
 * @author Hanyuu Furude
 */
public interface SimpleScript
{
   /**Метод будет вызван после загрузки скрипта в Ханю*/
    public void onLoad();
    /**Возвращает описание скрипта*/
    public String getInfo();
}
