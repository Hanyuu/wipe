/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.ext;

import hanyuu.ext.interfaces.WipeExtension;
import java.io.File;

/**
 *
 * @author Hanyuu Furude
 */
public class ScriptContainer {

    public boolean use = true;
    public WipeExtension script;
    public File path;

    public ScriptContainer(WipeExtension script, File path) {
        this.script = script;
        this.path = path;
    }

    @Override
    public String toString() {
        return script.getInfo();
    }
}
