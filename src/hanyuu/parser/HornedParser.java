/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.parser;

import java.io.InputStream;
import org.htmlparser.lexer.Page;
import org.htmlparser.lexer.Lexer;
import org.htmlparser.Parser;
import org.htmlparser.util.NodeIterator;
import org.htmlparser.util.NodeList;
import org.htmlparser.Node;
import org.htmlparser.Text;
import org.htmlparser.filters.TagNameFilter;
import java.util.ArrayList;
import org.htmlparser.Tag;
import hanyuu.managers.pictures.Pic;
import hanyuu.net.wipe.AbstractWipe;
import utils.Constants;
import config.Config;
import java.util.regex.Pattern;

/**
 *
 * @author Hanyuu Furude
 */
public class HornedParser implements Constants {

    private Parser parser= new Parser();;
    private final TagNameFilter a = new TagNameFilter("a");
    private final TagNameFilter span = new TagNameFilter("span");
    private final TagNameFilter p = new TagNameFilter("p");
    private ArrayList<String> results=new ArrayList<String>();

    private static void parse(InputStream in) {
        try {
            int i;
            while ((i = in.read()) != -1) {
                System.out.print((char) i);
            }
        } catch (Exception e) {
        }

    }
    @SuppressWarnings("CallToThreadDumpStack")
    public ArrayList<String> getThreads(InputStream in,AbstractWipe wipe) {
        try {
            parser.setLexer(new Lexer(new Page(in, encoding)));
            Node[] al = parser.extractAllNodesThatMatch(a).toNodeArray();
            String h;
            results.clear();
            for (Node n : al) {
                h = n.toHtml();
                if (h.contains(wipe.getChan().BoardReplayTag)) {                 
                    results.add(rmchars.split(h)[1]);
                }
            }
            return results;
        } catch (Exception e) {
            wipe.setException(e);
            wipe.getThreadManager().handleError(wipe);
        }
        return null;
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public ArrayList<String> getPosts(InputStream in,AbstractWipe wipe) {
        try {
            parser.setLexer(new Lexer(new Page(in, encoding)));
            Node[] al = parser.extractAllNodesThatMatch(a).toNodeArray();
            String h;
            results.clear();
            for (Node n : al) {
                h = n.toHtml().replaceAll("</a>", "");
                //p(h);
            }
            return results;
        } catch (Exception e) {
            wipe.setException(e);
            wipe.getThreadManager().handleError(wipe);
        }
        return null;
    }

    @SuppressWarnings("CallToThreadDumpStack")
    /**
     * И здесь тоже всё надо переделать.Костыли склеенные скотчем, бля.
     */
    public String getMyPost(InputStream in,AbstractWipe wipe) {
        try {
            parser.setLexer(new Lexer(new Page(in, encoding)));

            
            Node[] spans = parser.extractAllNodesThatMatch(span).toNodeArray();
            Tag tmp;
            boolean found=false;
            String plain;
            String[] splits;
            String attribute;
            String id;
            for (Node n : spans) {
                tmp = (Tag) n;
                attribute = tmp.getAttribute("class");
                plain = tmp.toPlainTextString();
                if (attribute != null && attribute.contains("reflink"))
                {
                        splits=rmchars.split(plain);
                        id=splits[splits.length-1];
                        for(Node nn:n.getParent().getChildren().toNodeArray())
                        {
                            if(nn instanceof Tag)
                            {
                                tmp=(Tag)nn;
                                attribute = tmp.getAttribute("class");
                                plain=nn.toPlainTextString();
                                if(attribute != null && attribute.contains("filesize"))
                                {
                                    //p(plain);
                                    found|=(plain.contains(wipe.getFile().w_s)&&plain.contains(wipe.getFile().h_s));
                                }
                                /*
                                
                                p("pic "+id+" "+found);
                                found|=plain.contains(wipe.getMsg());
                                p("msg "+id+" "+found);
                                found|=plain.contains(Config.name);
                                p("name"+id+" "+found);
                                found|=plain.contains(Config.theme);*/
                                if(found&&wipe.getThreadManager().getMM().add(id))
                                    return id;
                                }
                        }
                }
            }
        } catch (Exception e) {
            wipe.setException(e);
            wipe.getThreadManager().handleError(wipe);
        }
        return null;
    }
    private void p(Object o) {
        System.out.println(o);
    }
}
