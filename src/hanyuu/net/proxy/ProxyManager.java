/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.net.proxy;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;


import java.io.BufferedReader;
import java.io.RandomAccessFile;

import java.net.Socket;
import java.net.InetSocketAddress;

import hanyuu.managers.ThreadManager;
import hanyuu.net.HTTPClient;
import config.Config;
import utils.Constants;
import ui.interfaces.UI;
import utils.Utils;
/**
 *
 * @author Hanyuu Furude
 * Вообщем тут пиздец, и он работает через раз.<br/>
 * Долгое и вдумчивое переписывание не помогло.
 */
public class ProxyManager extends ArrayList<HttpProxy> implements Constants {

    private String proxysFile = "./ini/proxy.ini";
    private final String comment = "#Формат\n#proxy:port\n#или\n#proxy:port:login:password\n";
    private boolean file = false;
    private int pos = 0;
    public int progress = 1;
    private UI ui = null;

    @SuppressWarnings("CallToThreadDumpStack")
    public ProxyManager() {
        if (!(new File(proxysFile).exists())) {
            System.out.println("ProxyManager, file " + proxysFile + " not found.");
            System.exit(-1);
        }
        load(proxysFile);

    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void addProxy(HttpProxy p) {
        this.add(p);
        try {
            RandomAccessFile raf = new RandomAccessFile(proxysFile, "rw");
            raf.skipBytes((int) raf.length());
            raf.writeBytes(p.toString() + "\n");
            raf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @SuppressWarnings("CallToThreadDumpStack")
    public void load(String file) {
        try {
            FileReader fr = new FileReader(file);
            String s;
            BufferedReader br = new BufferedReader(fr);

            while ((s = br.readLine()) != null) {
                pos++;
                if (s.startsWith("#") || s.isEmpty() || s.length() < 5) {
                    continue;
                }
                s.replace(" ", "");
                StringTokenizer st = new StringTokenizer(s, ":");
                HttpProxy p = null;
                String host = st.nextToken();
                int port = Integer.parseInt(st.nextToken());
                /**Логин и пароль есть*/
                if (st.hasMoreTokens()) {
                    String login = st.nextToken();
                    String password = st.nextToken();

                    if (!isInList(host)) {
                        p = new HttpProxy(host, port, login, password);
                    }
                } /**Логина-пароля нету*/
                else {
                    if (!isInList(host)) {
                        p = new HttpProxy(host, port);
                    }
                }
                if (p != null) {
                    add(p);
                }
                if (Config.checkOnLoad) {
                    for (int i = 0; i < size(); i++) {
                        System.out.print("[" + i + "]");
                        if (Config.checker) {
                            checker();
                        }
                    }
                }
            }
            fr.close();
            br.close();
            saveFile();
        } catch (Exception e) {
            System.err.println("Position is: " + pos);
            e.printStackTrace();
        }
    }

    private void checker() {
        for (HttpProxy p : this) {
            check(p, null);
        }
    }

    private boolean isInList(String host) {
        for (HttpProxy h : this) {
            if (h.getHost().contains(host)) {
                System.out.println("Warning! Proxy " + host + " duplicated!");
                return true;
            }
        }
        return false;
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void writeChecked(HttpProxy p) {
        synchronized (lock) {
            try {
                File f = new File("./checked_proxy.txt");
                if (!f.exists()) {
                    f.createNewFile();
                }

                RandomAccessFile raf = new RandomAccessFile(f, "rw");
                raf.skipBytes((int) raf.length());

                raf.writeBytes(p.toString() + "\n");
                raf.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings({"CallToNativeMethodWhileLocked", "CallToThreadDumpStack"})
    public void saveFile() {
        try {
            if (file) {
                Thread.sleep(250);
            }
            synchronized (lock) {
                File p = new File(proxysFile);
                file = true;
                p.delete();
                FileWriter fr = new FileWriter(proxysFile);
                BufferedWriter bw = new BufferedWriter(fr);

                bw.write(comment);
                for (HttpProxy proxy : this) {
                    bw.write(proxy.toString() + "\n");
                }
                bw.close();
                fr.close();
                file = false;
            }
        } catch (Exception e) {
            file = false;
            e.printStackTrace();
        }
    }

    public void check(final HttpProxy proxy, final UI u) {
        System.out.println("Checking: " + proxy);
        this.ui = u;
        Utils.startThread((new Runnable() {

            public void run() {
                if (Config.emulateWipe) {
                    ThreadManager tm = ThreadManager.getInstance();
                    tm.runWipe(proxy);
                }
                if (Config.contentCheck && simple(proxy)) {
                    if (!full(proxy)) {
                        delete(proxy, ui);
                    }
                } else if (!simple(proxy)) {
                    delete(proxy, ui);
                }

                if (ui != null) {
                    ui.setProxyProgress(progress++);
                }
                writeChecked(proxy);
            }
        }));
    }

    public void check(String host, int port) {
        HttpProxy p = new HttpProxy(host, port);
        check(p, null);
    }

    public void delete(HttpProxy proxy, UI ui) {
        synchronized (lock) {
            remove(proxy);
            if (ui != null) {
                ui.removeProxy(proxy);
            }
            saveFile();
            System.out.println("deleted " + proxy.getHost());
        }
    }

    private boolean simple(HttpProxy proxy) {
        try {
            Socket s = new Socket();
            s.connect(new InetSocketAddress(proxy.getHost(), proxy.getPort()), 1000);
            boolean result = s.isConnected();
            s.close();
            if (result) {
                if (ui != null) {
                    ui.logInfo(proxy + " работает.");
                } else {
                    System.out.println(proxy + " work");
                }
            }
            return result;
        } catch (Exception e) {
            if (ui != null) {
                ui.logError(proxy + " не работает.");
            } else {
                System.out.println(proxy + " dont work");
            }
            return false;
        }
    }

    @SuppressWarnings("CallToThreadDumpStack")
    private boolean full(HttpProxy proxy) {
        try {

            HTTPClient http = new HTTPClient(proxy);
            BufferedReader br = http.getBufferedReader(Config.syte);
            String s = "", s2 = "";
            while ((s = br.readLine()) != null) {
                s2 += s;
            }
            if (s2.contains(Config.syteKey)) {
                if (ui != null) {
                    ui.logInfo(proxy + " ответ от " + Config.syte + " получен.");
                } else {
                    System.out.println(proxy + " all ok, proxy work");
                }
                return true;
            } else {
                if (ui != null) {
                    ui.logError(proxy + " ответ от " + Config.syte + " не получен.");
                } else {
                    System.out.println(proxy + " bad content.");
                }
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (ui != null) {
                ui.logError(proxy + " ответ от " + Config.syte + " не получен.");
            } else {
                System.out.println(proxy + " bad content.");
            }
            return false;
        }
    }
}
