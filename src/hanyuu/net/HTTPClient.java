/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.net;

import hanyuu.net.wipe.Wipe;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.StringTokenizer;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpVersion;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.params.HttpParams;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLSocketFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;
import javax.net.ssl.TrustManager;
import java.security.cert.X509Certificate;
import java.security.cert.CertificateException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import hanyuu.net.proxy.HttpProxy;
import hanyuu.ext.ScriptContainer;
import sun.awt.image.BytePackedRaster;
import ui.interfaces.UI;
import org.apache.commons.io.IOUtils;
/**
 *
 * @author Hanyuu Furude
 */
public class HTTPClient {

    private String proxyHost, proxyLogin, proxyPassword;
    private int proxyPort = -1;
    private Wipe wipe=null;
    private String cookies="";
    private HttpHost proxy=null;
    private UI ui;
    private DefaultHttpClient lastClient;
    public HTTPClient(HttpProxy proxy, Wipe wipe) {
        this.proxyHost = proxy.getHost();
        this.proxyPort = proxy.getPort();
        this.wipe = wipe;
        this.proxy = proxy.getProxy();
        if (proxy.isAuth()) {
            this.proxyLogin = proxy.getLogin();
            this.proxyPassword = proxy.getPassword();
        }
        ui=wipe.getThreadManager().getUIManager().getUI();
    }

    public HTTPClient(HttpProxy proxy) {
        this.proxyHost = proxy.getHost();
        this.proxyPort = proxy.getPort();
        this.proxy = proxy.getProxy();
        if (proxy.isAuth()) {
            this.proxyLogin = proxy.getLogin();
            this.proxyPassword = proxy.getPassword();
        }
    }

    /**
     *
     * @param RequestUrl
     * @param host
     * @para headers
     * @param saveCookies
     * @return
     */
    @SuppressWarnings("CallToThreadDumpStack")
    public BufferedReader getBufferedReader(String RequestUrl, HttpHost host, ArrayList<String> headers, boolean saveCookies) {
        try {
            return new BufferedReader(new InputStreamReader(getInputStream(RequestUrl, host, headers, saveCookies), Wipe.encoding));
        } catch (Exception e) {
            
            if (wipe != null) {
                wipe.setException(e);
                wipe.getThreadManager().handleError(wipe);
            }
            else{
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     *
     * @param RequestUrl
     * @param saveCookies
     * @return
     */
    public BufferedReader getBufferedReader(String RequestUrl, boolean saveCookies) {
        return getBufferedReader(RequestUrl, null, null, saveCookies);
    }

    public byte[] get(String url){
        try{
            return IOUtils.toByteArray(this.getInputStream(url));
        }catch(Exception e){
            return null;
        }
    }
    /**
     *
     * @param RequestUrl
     * @return
     */
    public BufferedReader getBufferedReader(String RequestUrl) {
        return getBufferedReader(RequestUrl, null, null, false);
    }

    /**
     *
     * @param RequestUrl
     * @param headers
     * @return
     */
    public BufferedReader getBufferedReader(String RequestUrl, ArrayList<String> headers) {
        return getBufferedReader(RequestUrl, null, headers, false);
    }

    /**
     *
     * @param RequestUrl
     * @param save cookies
     * @return
     */
    public InputStream getInputStream(String RequestUrl, boolean saveCookies) {
        return getInputStream(RequestUrl, null, null, saveCookies);
    }

    /**
     *
     * @param RequestUrl
     * @return
     */
    public InputStream getInputStream(String RequestUrl) {
        return getInputStream(RequestUrl, null, null, false);
    }

    /**
     *
     * @param RequestUrl
     * @param host
     * @param headers
     * @param save cookies
     * @return
     */
    @SuppressWarnings("CallToThreadDumpStack")
    public InputStream getInputStream(String RequestUrl, HttpHost host, ArrayList<String> headers, boolean saveCookies) {
        try {
            if (wipe != null) {
                for (ScriptContainer sc : wipe.getThreadManager().getScripts().getScripts()) {
                    if (sc.use) {
                        Object[] o = sc.script.onGetRequest(wipe, RequestUrl, host, headers, saveCookies);
                        if (o != null && o.length == 4) {
                            RequestUrl = (String) o[0];
                            host = (HttpHost) o[1];
                            headers = (ArrayList<String>) o[2];
                            saveCookies = (Boolean) o[3];
                        }
                    }
                }
            }
            HttpGet get = new HttpGet(RequestUrl);
            HttpResponse hp;
            if (headers != null) {
                for (String header : headers) {
                    StringTokenizer st = new StringTokenizer(header, ",");
                    get.addHeader(st.nextToken(), st.nextToken());
                }
            }
                        
            if (host == null) {
                hp = createClient().execute(get);
            } else {
                hp = createClient().execute(host, get);
            }

            if (saveCookies) {
                saveCookies(hp);
            }
            if (wipe != null) {
                for (ScriptContainer sc : wipe.getThreadManager().getScripts().getScripts()) {
                    if (sc.use) {
                        HttpResponse schp = sc.script.onGetAnswer(wipe, hp);
                        if (schp != null) {
                            hp = schp;
                        }
                    }
                }
            }
            InputStream result=null;
            Header encoding=hp.getEntity().getContentEncoding();
            if(encoding!=null&&encoding.getValue().contains("gzip")){
               result=new GZIPInputStream(hp.getEntity().getContent()); 
            }else{
                result=hp.getEntity().getContent();
            }
            if(result==null&&wipe!=null)
            {
                wipe.destroy("Answer from "+RequestUrl+" is null!");
                ui.removeThread(null);
                wipe.getThreadManager().getProxyManager().delete(wipe.getProxy(), ui);
            }
            return result;
        } catch (Exception e) {

            if (wipe != null) {
                wipe.setException(e);
                wipe.getThreadManager().handleError(wipe);
            }
            else{
                e.printStackTrace();
            }
        }
        return null;
    }

    private void saveCookies(HttpResponse hp) {
        cookies = "";
        for (Header hh : hp.getAllHeaders()) {
            if (hh.getName().contains("ookie")) {
                //System.out.println("save:"+hh.getName()+":"+hh.getValue());
                String tmp = hh.getValue();
                if (tmp.endsWith(";")) {
                    cookies += tmp;
                } else {
                    cookies += tmp + ";";
                }
                tmp = null;
            }
        }
    }

    public void addCookie(String cookie)
    {
        if(cookie.endsWith(";"))
            cookies+=cookie;
        else
            cookies+=cookie+";";
    }
    public String getCookieByName(String name) {
        StringTokenizer st = new StringTokenizer(cookies, ";");
        while (st.hasMoreTokens()) {
            String s = st.nextToken();
            if (s.startsWith(name)) {
                StringTokenizer ct = new StringTokenizer(s, "=");
                ct.nextToken();
                return ct.nextToken();
            }
        }
        return "";
    }

    public String getCookies() {
        return cookies;
    }

    /**
     *
     * @param url
     * @param host
     * @param me
     * @return
     */
    public BufferedReader postBufferedReader(String url, HttpHost host, MultipartEntity me) {
        return postBufferedReader(url, host, me, null);
    }

    /**
     *
     * @param url
     * @param me
     * @return
     */
    public BufferedReader postBufferedReader(String url, MultipartEntity me) {
        return postBufferedReader(url, null, me, null);
    }

    /**
     *
     * @param url
     * @param me
     * @para headers
     * @return
     */
    public BufferedReader postBufferedReader(String url, MultipartEntity me, ArrayList<String> headers) {
        return postBufferedReader(url, null, me, headers);
    }

    /**
     *
     * @param url
     * @param host
     * @param me
     * @param headers
     * @return
     */
    public BufferedReader postBufferedReader(String url, HttpHost host, MultipartEntity me, ArrayList<String> headers) {
        try {
            return new BufferedReader(new InputStreamReader(post(url, me, headers), Wipe.encoding));
        } catch (Exception e) {
            if (wipe == null) {
                return null;
            }
            wipe.setException(e);
            wipe.getThreadManager().handleError(wipe);

        }
        return null;
    }

    /**
     *
     * @param url
     * @param me
     * @return
     */
    public InputStream post(String url, MultipartEntity me) {
        return post(url, null, me, null);
    }

    /**
     *
     * @param url
     * @param me
     * @param headers
     * @return
     */
    public InputStream post(String url, MultipartEntity me, ArrayList<String> headers) {
        return post(url, null, me, headers);
    }
    public BufferedReader postBufferedReader(String url, HttpHost host, String value, ArrayList<String> headers) {
        try {
            return new BufferedReader(new InputStreamReader(post(url, host, value, headers), Wipe.encoding));
        } catch (Exception e) {
            if (wipe == null) {
                return null;
            }
            wipe.setException(e);
            wipe.getThreadManager().handleError(wipe);

        }
        return null;
    }
    @SuppressWarnings("CallToThreadDumpStack")
    public InputStream post(String url, HttpHost host, String value, ArrayList<String> sheaders) {
        try {

            HttpPost post = new HttpPost(url);
            if (sheaders != null) {
                for (String header : sheaders) {
                    StringTokenizer st = new StringTokenizer(header, ",");
                    post.addHeader(st.nextToken(), st.nextToken());
                }
            }

            StringEntity postEntity = new StringEntity(value, "UTF-8");
            postEntity.setContentType("application/x-www-form-urlencoded");
            post.setEntity(postEntity);

            HttpResponse hp;
            if (host == null) {
                hp = createClient().execute(post);
            } else {
                hp = createClient().execute(host, post);
            }

            saveCookies(hp);
            return hp.getEntity().getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     *
     * @param url
     * @param host
     * @param me
     * @param String headers
     * @return
     */
    @SuppressWarnings("CallToThreadDumpStack")
    public InputStream post(String url, HttpHost host, MultipartEntity me, ArrayList<String> sheaders) {
        try {
            if (wipe != null) {
                for (ScriptContainer sc : wipe.getThreadManager().getScripts().getScripts()) {
                    if (sc.use) {
                        Object[] o = sc.script.onPostRequest(wipe, url, host, me, sheaders);
                        if (o != null && o.length == 4) {
                            url = (String) o[0];
                            host = (HttpHost) o[1];
                            me = (MultipartEntity) o[2];
                            sheaders = (ArrayList<String>) o[3];
                        }

                    }
                }
            }
            HttpPost post = new HttpPost(url);

            if (sheaders != null) {
                for (String header : sheaders) {
                    StringTokenizer st = new StringTokenizer(header, ",");
                    post.addHeader(st.nextToken(), st.nextToken());
                }
            }

            if (cookies != null) {
                post.addHeader("Cookie", cookies);
                cookies = "";
            }

            post.setEntity(me);
            HttpResponse hp;

            if (host == null) {
                hp = createClient().execute(post);
            } else {
                hp = createClient().execute(host, post);
            }
            saveCookies(hp);

            if (wipe != null) {
                for (ScriptContainer sc : wipe.getThreadManager().getScripts().getScripts()) {
                    if (sc.use) {
                        HttpResponse schp = sc.script.onGetAnswer(wipe, hp);
                        if (schp != null) {
                            hp = schp;
                        }
                    }
                }
            }
            InputStream result=hp.getEntity().getContent();
            if(result==null&&wipe!=null)
            {
                wipe.destroy("Answer from "+url+" is null!");
                ui.removeThread(null);
                wipe.getThreadManager().getProxyManager().delete(wipe.getProxy(), ui);
            }
            return result;
        } catch (Exception e) {
            
            if (wipe != null) {
                wipe.setException(e);
                wipe.getThreadManager().handleError(wipe);
            }else
            {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     *
     * @return true if proxy enable
     */
    public boolean isViaProxy() {
        return proxyHost != null && proxyPort > 0;
    }

    /**
     *
     * @return
     */
    public boolean isProxyAuth() {
        return proxyLogin != null && proxyPassword != null;
    }

    /**
     * Create HTTP Client
     * @return
     */
    @SuppressWarnings("CallToThreadDumpStack")
    public DefaultHttpClient createClient() {
        DefaultHttpClient httpclient = null;
        try {
            if(lastClient!=null)
                lastClient.getConnectionManager().shutdown();
            SchemeRegistry supportedSchemes = new SchemeRegistry();

            supportedSchemes.register(new Scheme("http",
                    PlainSocketFactory.getSocketFactory(), 80));

            SSLSocketFactory s=new SSLSocketFactory(createSSLContext());
            supportedSchemes.register(new Scheme("https",
                    s, 443));
            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            if (wipe != null) {
                HttpProtocolParams.setUserAgent(params, wipe.getUserAgent());
            } else {
                HttpProtocolParams.setUserAgent(params, Wipe.UserAgent);
            }
            HttpProtocolParams.setContentCharset(params, Wipe.encoding);
            HttpProtocolParams.setUseExpectContinue(params, false);

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params,
                    supportedSchemes);
            httpclient = new DefaultHttpClient(ccm, params);
            if(isViaProxy())
                httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
            if (isProxyAuth()) {
                httpclient.getCredentialsProvider().setCredentials(
                        new AuthScope(proxyHost, proxyPort),
                        new UsernamePasswordCredentials(proxyLogin, proxyPassword));
            }
        } catch (Exception e) {
            if (wipe != null) {
                wipe.setException(e);
                wipe.getThreadManager().handleError(wipe);
            }else{
                e.printStackTrace();
            }
        }
        lastClient=httpclient;
        return lastClient;
    }
    private SSLContext createSSLContext()throws KeyManagementException, NoSuchAlgorithmException
    {
        SSLContext ctx = SSLContext.getInstance("TLS");
            X509TrustManager tm = new X509TrustManager() {

                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };
            ctx.init(null, new TrustManager[]{tm}, null);
            return ctx;
    }
}
