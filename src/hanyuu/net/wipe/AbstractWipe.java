/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hanyuu.net.wipe;


import config.Config;
import hanyuu.ext.ScriptContainer;
import java.io.File;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import hanyuu.chan.Chan;
import ui.interfaces.UI;
import hanyuu.managers.ThreadManager;
import hanyuu.managers.pictures.Pic;
import hanyuu.net.proxy.HttpProxy;
import ui.interfaces.ITray;
import java.io.InputStream;
import hanyuu.parser.HornedParser;
import hanyuu.ext.interfaces.OCR;
import hanyuu.net.HTTPClient;
import org.apache.http.entity.mime.MultipartEntity;
/**
 *
 * @author Hanyuu Furude
 * This class provides simple methods for all wipe objects;
 */
public abstract class AbstractWipe {

    protected HTTPClient httpClient;
    protected Pic picture;
    protected HttpProxy proxy;
    protected String txtCaptcha;
    protected UI ui;
    protected Thread wipeThread;
    protected boolean work = true;
    protected ImageIcon captcha = null;
    protected String status;
    protected ITray tray;
    protected Chan chan;
    protected boolean ready = true;
    protected boolean mustSleep = false;
    protected boolean error = false;
    protected String cookies;
    protected String msg;
    protected String url;
    protected ThreadManager tm;
    protected String WipeUserAgent = Config.userAgent;

    protected int successful = 0;
    protected int failed = 0;
    protected String thread;
    protected ArrayList<String> headers;
    protected ArrayList<String> sendedThreads = new ArrayList<String>();
    protected ArrayList<String> parsedThreads = new ArrayList<String>();
    protected boolean onZeroPage = false;
    protected byte errors = 0;
    protected boolean isSleeping;
    protected String lastPost;

    protected HornedParser parser;
    protected OCR ocr;
    protected MultipartEntity me;
    protected Action lastAction;
    protected Exception exception;
    protected void count() {
        tray.reIcon(tm.getAllSuccessful(), tm.getAllFailed());
        ui.setSuccessful(tm.getAllSuccessful());
        ui.setFailed(tm.getAllFailed());
    }

    public void destroy(String reason) {
        tm.destroyThread(this, reason);
    }

    public String getCaptcha() {
        return txtCaptcha;
    }

    public ImageIcon getCaptchaImg() {
        return captcha;
    }

    public Chan getChan() {
        return chan;
    }
    public void setLastAction(Action action)
    {
        lastAction=action;
    }
    public Action getLastAction()
    {
       return lastAction;
    }
    public OCR getOCR()
    {
        return ocr;
    }
    public void setOCR(OCR ocr)
    {
        this.ocr=ocr;
    }
    public int getFailed() {
        return failed;
    }

    public Pic getFile() {
        return picture;
    }

    public HTTPClient getHttpClient() {
        return httpClient;
    }

    public String getActiveThread(){
        return thread;
    }
    public String getMsg() {
        return msg;
    }

    public HttpProxy getProxy() {
        return proxy;
    }

    public String getStatus() {
        return status;
    }

    public int getSuccessful() {
        return successful;
    }

    public Thread getThread() {
        return wipeThread;
    }

    public ThreadManager getThreadManager() {
        return tm;
    }   

    public UI getUI() {
        return ui;
    }

    public String getUserAgent() {
        return WipeUserAgent;
    }

    public boolean isError() {
        return error;
    }

    public boolean isWork() {
        return work;
    }

    public synchronized void myNotify() {
        ready = true;
        notify();
    }

    public void waitForNotReady() {
        try {
            ready=false;
            synchronized (this) {
                ui.logInfo("Thread [ "+toString()+" ] waiting...");
                while (!ready) {
                    wait();
                }
                ui.logInfo("Notifyng thread [ "+toString()+" ] ...");
            }
            
        } catch (Exception e) {
            waitForNotReady();
        }
    }
    public void setCaptcha(String captcha) {
        txtCaptcha = captcha;
        if(Config.waitForNotReady)
            tm.removeFromNotReady(this);
    }

    public void setCaptchaImg(ImageIcon img) {
        captcha = img;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public void setFile(File file) {
        this.picture = new Pic(file);
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setProxy(HttpProxy p) {
        proxy = p;
    }

    public void setReadyCaptcha(boolean ready) {
        this.ready = ready;
    }

    public void setStatus(String st) {
        status = st;
    }

    public void setUserAgent(String agent) {
        WipeUserAgent = agent;
    }
    public Exception getException()
    {
        return exception;
    }
    public void setSleeping(boolean sleep)
    {
        isSleeping=sleep;
    }
    public boolean isSleeping()
    {
        return isSleeping;
    }
    public void setException(Exception e)
    {
        e.printStackTrace();
        exception=e;
    }
    @Override
    public String toString() {
        return proxy.getHost();
    }
    
    public synchronized void stop() {

        if (!work) {
            return;
        }
        work = false;
        captcha = null;
        if ((Config.useTmp || Config.randomPicGenerate) && (picture != null && picture.exists())) {
            picture.delete();
        }
        ui.removeThread(this);
        for (ScriptContainer s : tm.getScripts().getScripts()) {
            if (s.use) {
                s.script.onStopWipe(this);
            }
        }
        wipeThread.setPriority(Thread.MIN_PRIORITY);
    }
    public byte getErrors()
    {
        return errors;
    }
    public void incErrors()
    {
        errors++;
    }
    public void requestDelet(String[] msgs)
    {
        lastAction=Action.DeletingPost;
        delete(msgs);
    }

    public abstract void delete(String[] msgs);
    public abstract void post();
    public abstract InputStream getCaptchaStream();
    public abstract ArrayList<String> getThreads();
}
