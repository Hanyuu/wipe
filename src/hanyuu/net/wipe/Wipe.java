/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.net.wipe;

import java.io.InputStream;

import java.util.ArrayList;

import java.io.BufferedReader;
import utils.Utils;
import utils.Constants;
import hanyuu.chan.Chan;
import hanyuu.net.proxy.HttpProxy;
import config.Config;
import config.WorkMode;
import hanyuu.managers.ThreadManager;
import hanyuu.ext.ScriptContainer;
import hanyuu.net.HTTPClient;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.ContentBody;
/**
 *
 * @author Hanyuu Furude
 */
@SuppressWarnings({"unchecked", "CallToThreadDumpStack"})
public abstract class Wipe extends AbstractWipe implements Runnable, Constants {
   

    @SuppressWarnings("CallToThreadStartDuringObjectConstruction")
    public Wipe(HttpProxy proxy, ThreadManager tm, Chan chan) {
        this.chan = chan;
        this.proxy = proxy;
        this.tm = tm;
        ui = tm.getUIManager().getUI();
        tray = tm.getUIManager().getTray();
        ocr = tm.getScripts().getOCRbyName(Config.ocrMode);
        httpClient = new HTTPClient(proxy, this);
        parser = tm.getParser();
        if (chan.ChanHost != null && chan.ScriptPath != null) {
            url = chan.ScriptPath.replace("[%b]", Config.board);
        }
        this.msg = Config.msg;
        thread = Config.thread;
        wipeThread = new Thread(this, proxy.getHost());
        wipeThread.start();
    }

    protected boolean onZeroPage() {

        if (!(Config.workMode == WorkMode.OnZeroPage)) {
            onZeroPage = false;
        } else if (getThreads().contains(Config.thread)) {
            ui.logInfo("Thread " + thread + " on zero page.");
            onZeroPage = true;
        } else {
            onZeroPage = false;
        }
        return onZeroPage;
    }

    

    public boolean checkPost(String s) {
        //System.out.println(s);
        if (!work) {
            return true;
        }
        if ((picture != null && picture.exists()) && (Config.useTmp || Config.randomPicGenerate)) {
            picture.delete();
        }
        if (Config.parser) {
            lastPost = parser.getMyPost(httpClient.getInputStream(chan.ChanHost + Config.board),this);
            if (lastPost == null || lastPost.isEmpty()) {
                if (!isError(s)) {
                    failed++;
                    error = true;
                    ui.logError("Неудалось найти сообщение, причина неизвестна. Ниже приведён ответ от борды:\n" + s);
                }
                count();
                return false;
            } else {
                onSuccess();
                //tm.addMessage(lastPost);
                return true;
            }
        }
        if (isError(s)) {
            count();
            return false;
        } else {
            if (chan.MessageSuccessful != null && chan.MessageSuccessful.matcher(s).find()) {
                onSuccess();
                return true;
            }

        }
        onSuccess();
        return true;
    }

    public void onSuccess() {
        mustSleep = true;
        successful++;
        tm.getImgManager().setUsedFile(picture);
        tray.trayPrint("Успешно " + Config.chanName, "Уже " + tm.getAllSuccessful() + " постов");
        ui.logInfo("Post " + (Config.parser ? "(ID: " + lastPost + ")" : "") + " successful " + tm.getAllSuccessful() + " Proxy is: " + proxy.getHost() + " Threads count " + tm.size());
        ocr.onSuccess();
        count();
        if (Config.msgCount && tm.getAllSuccessful() >= Config.msgCountInt) {
            ui.logInfo("Поток " + wipeThread.getName() + " завершён. Все сообщения (" + Config.msgCountInt + ") отправленны.");
            destroy("All massage sended.");
        }
    }

    private boolean isError(String msg) {
        boolean isError = false;
        //System.out.println(msg);
        if (chan.MessageBan != null && chan.MessageBan.matcher(msg).find()||msg.contains("proxy")) {
            ui.logError(wipeThread.getName() + " banned!");
            tm.getProxyManager().delete(proxy, ui);
            tm.destroyThread(this, "Banned!");
            isError = true;
        }
        /*if (msg.contains("Error:") || msg.contains("error")) {
            ui.logError(msg.substring(msg.indexOf("rror") - 1));
            isError = true;
        }*/
        if (chan.MessageInvalidCaptcha != null && chan.MessageInvalidCaptcha.matcher(msg).find()) {
            ui.logError("Неправельная капча.");
            ocr.onError();
            isError = true;
        }
        if (chan.MessageError != null && chan.MessageError.matcher(msg).find() && !isError) {
            if (Config.emulateWipe) {
                destroy("Эмуляция вайпа удачна.");
                return false;
            }
            
            int index = msg.indexOf(chan.MessageError.toString());
            if (index > 0) {
                ui.logError(msg.substring(index));
            }
            isError = true;
        }
        if (isError) {
            failed++;
            tray.trayPrintError("Неудачно", "Всего " + tm.getAllFailed() + " неудачных постов");
        }
        mustSleep = !isError;
        return isError;
    }

    protected String getNextThread(ArrayList<String> ThreadArray) {
        if (!work) {
            return null;
        }
        String threadNumber="";
        try{
        threadNumber = parsedThreads.get(random.nextInt(parsedThreads.size()));
        }catch(IllegalArgumentException iae)
        {
            destroy("Bad proxy. "+iae.toString());
        }
        if (sendedThreads.contains(threadNumber)) {
            getNextThread(ThreadArray);
        }
        sendedThreads.add(threadNumber);
        return threadNumber;
    }

    public void run() 
    {
        
        while (work && ready) {
            System.gc();
            for (ScriptContainer s : tm.getScripts().getScripts()) {
                if (s.use) {
                    s.script.onRunWipe(this);
                }
            }
            if (!Config.BokuNoFile) {
                tm.getImgManager().reImg(this);
            }
                post();
            if (Config.timeout > 0 && (mustSleep || ((Config.workMode == WorkMode.OnZeroPage) && onZeroPage)) && work) {
                ui.logInfo("Sleeping " + Config.timeout + " ms."
                        + " Thread: " + wipeThread.getName());
                tm.sleepWipe(Config.timeout,this);
                mustSleep = false;
            }
            if (!(Config.workMode == WorkMode.OnZeroPage) && (Config.useTmp || Config.randomPicGenerate) && (picture != null && picture.exists())) {
                picture.delete();
            }
            if (!(Config.workMode == WorkMode.OnZeroPage) && !Config.onePic && !Config.BokuNoFile) {
                picture = tm.getImgManager().getFile(this);
            }
        }
    }
    public void post() {
        lastAction=Action.Posting;
        try {
            if (!work) {
                return;
            }
            thread = Config.thread;
            if(onZeroPage())
                return;
            headers = new ArrayList<>();
            me = new MultipartEntity();
            makeMimesAndHeaders(url);

            ocr.recognizeCaptcha(this);
            me.addPart(chan.FieldCaptcha, Utils.sb(txtCaptcha));

            if ((Config.pasteNoMsg && Config.pasteOnPic) && !Config.BokuNoFile) {
                me.addPart(chan.FieldMessage, Utils.sb(""));
            } else {
                me.addPart(chan.FieldMessage, Utils.sb(msg));
            }

                me.addPart(chan.FieldEmail, Utils.sb(Config.email));
 

            me.addPart(chan.FieldName, Utils.sb(Config.name));
            me.addPart(chan.FieldTheme, Utils.sb(Config.theme));
            me.addPart(chan.FieldPassword, Utils.sb(Config.password));
            //p(chan.FieldPassword+":"+Config.password);

            if (picture == null || Config.BokuNoFile) {
                me.addPart(chan.FieldFile, Utils.sb(""));
            } else {
                ContentBody cb = new FileBody(picture, "image/" + picture.format);
                me.addPart(chan.FieldFile, cb);
            }

            if (!work) {
                return;
            }
            if(Config.waitForNotReady&&!tm.isReady())
                waitForNotReady();

            BufferedReader br = httpClient.postBufferedReader(url, me, headers);
            String s, s2 = "";
            while ((s = br.readLine()) != null) {
                s2 += s;
            }
            //System.out.println(s2);
            if (checkPost(s2) && Config.silentBump) {
                ui.logInfo("Deleting post, number: " + lastPost);
                delete(new String[]{lastPost});
            }
        } catch (Exception e) {
            setException(e);
            tm.handleError(this);
        }
    }
    private static void p(Object o){
        System.out.println(o);
    }
    public void delete(String[] messages) {
        if (chan.RequestDeletePost == null || chan.RequestDeletePost.isEmpty()) {
            ui.showMessage("Удаление не возможно, не указан запрос на удаление.", 0);
            ui.logError("Cannot delete post, please configure chan " + chan.ChanName + " in you chans.xml");
            return;
        }
        try {
            for (final String m : messages) {
                final String request = chan.RequestDeletePost.replace("[%id]", m).replace("[%p]", Config.password).replace("[%b]", Config.board.replace("/", ""));
                System.out.println(request);
                Runnable r = new Runnable() {

                    public void run() {
                        Utils.print(httpClient.postBufferedReader(chan.ScriptPath.replace("[%b]", Config.board), null, request, headers));
                        /*if(result.contains(chan.MessageIncorrectPassword))
                        ui.logError("Message (ID: "+lastPost+") not deleted, incorrect password.");
                        else
                        ui.logInfo("Message (ID: "+lastPost+") not deleted.");*/
                    }
                };
                Utils.startThread(r);
            }
        } catch (Exception e) {
            e.printStackTrace();

            tm.handleError(this);
        }
    }

    public ArrayList<String> getThreads() {
        lastAction=Action.RequestThreads;
        try {
            if (!(Config.workMode == WorkMode.OnZeroPage)) {
                if (sendedThreads.size() < parsedThreads.size()) {
                    return parsedThreads;
                } else {
                    sendedThreads.clear();
                }
            }
            String threadsUrl;
            headers = new ArrayList<String>();
            if (Config.page == 0 || (Config.workMode == WorkMode.OnZeroPage)) {
                threadsUrl = chan.ChanPosts.replace("[%host]", chan.ChanHost).replace("[%b]", Config.board);
            } else {
                threadsUrl = chan.ChanPosts.replace("[%host]", chan.ChanHost).replace("[%b]", Config.board) + Config.page + ".html";
            }
            parsedThreads.clear();
            ui.logInfo("Request threads array from " + chan.ChanHost + " via " + proxy);
            headers.add("Referer, " + chan.ChanHost + Config.board);
            parsedThreads = parser.getThreads(httpClient.getInputStream(threadsUrl, null, headers, true),this);
            ui.logInfo(parsedThreads.size() + " threas parsed, thread: "+toString());
            return parsedThreads;
        } catch (Exception e) {
            setException(e);
            tm.handleError(this);
        }
        return null;
    }
    
    public abstract void makeMimesAndHeaders(String url);

    public abstract InputStream getCaptchaStream();
}
