package hanyuu.net.wipe.boards;

import hanyuu.net.wipe.Wipe;
import hanyuu.net.proxy.HttpProxy;
import config.Config;
import utils.Utils;
import hanyuu.managers.ThreadManager;
import hanyuu.chan.Chan;

import java.io.InputStream;
import java.util.ArrayList;
import hanyuu.net.wipe.Action;
import org.apache.http.HttpHost;

/**
 *
 * @author Hanyuu Furude
 */
public class WakabaWipe extends Wipe {

    public WakabaWipe(HttpProxy proxy, ThreadManager tm, Chan chan) {
        super(proxy, tm, chan);
    }

    @Override
    @SuppressWarnings("CallToThreadDumpStack")
    public InputStream getCaptchaStream() {
        lastAction=Action.RequestCaptcha;
        if (!work) {
            return null;
        }
        String url;
        headers = new ArrayList<String>();
        
        switch (Config.workMode) {
            case WipeBoard:
                headers.add("Referer," + chan.ChanHost + Config.board);
                url = chan.CaptchaPath.replace("[%b]", Config.board).replace("[%r]", String.valueOf(Math.floor(Math.random() * 1000))).replace("[%t]", "mainpage").replace(".0", "").replace("[%host]", chan.ChanHost);
                break;
            default:
                headers.add("Referer," + chan.ChanHost + Config.board + "res/" + thread + ".html");
                url = chan.CaptchaPath.replace("[%b]", Config.board).replace("[%r]", String.valueOf(Math.floor(Math.random() * 1000))).replace("[%t]", "res" + thread).replace(".0", "").replace("[%host]", chan.ChanHost);
                break;
        }
        txtCaptcha = "";
        ui.logInfo("Request CAPTCHA from " + chan.ChanHost + " via " + proxy.toSipleString());
        headers.add("Cookie,wakabastyle=photon");
        System.out.println(url);
        return httpClient.getInputStream(url, null, headers, true);

    }

    @Override
    @SuppressWarnings("CallToThreadDumpStack")
    public void makeMimesAndHeaders(String url) {
        switch (Config.workMode) {
            /*case OnZeroPage:
                me.addPart(chan.retName, Utils.sb(chan.retName));
                headers.add("Referer," + chan.ChanHost + Config.board + "res/" + thread + ".html");
                headers.add("Cookie, wakabastyle=photon; email=; password=" + Config.password + "; name=; " + chan.retName + "=" + chan.retMode + ";");
                me.addPart(chan.FieldThreadNumber, Utils.sb(thread));
                ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".html");
                break;*/
            case WipeBoard:
                me.addPart(chan.FieldThreadNumber, Utils.sb(""));
                me.addPart(chan.retName, Utils.sb("board"));
                headers.add("Referer," + chan.ChanHost + Config.board);
                headers.add("Cookie,wakabastyle=photon; email=; password=" + Config.password + "; name=; " + chan.retName + "=board;");
                ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board);
                break;
            case Force:
                thread = getNextThread(getThreads());
                me.addPart(chan.FieldThreadNumber, Utils.sb(thread));
                me.addPart(chan.retName, Utils.sb(chan.retName));
                headers.add("Referer," + chan.ChanHost + Config.board + "res/" + thread + ".html");
                ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".html");
                break;
            case WipeThread:
            default:
                me.addPart(chan.retName, Utils.sb(chan.retName));
                headers.add("Referer," + chan.ChanHost + Config.board + "res/" + thread + ".html");
                headers.add("Cookie, wakabastyle=photon; email=; password=" + Config.password + "; name=; " + chan.retName + "=" + chan.retMode + ";");
                me.addPart(chan.FieldThreadNumber, Utils.sb(thread));
                ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".html");
                break;
        }

        me.addPart(chan.task, Utils.sb(chan.post));
        if (Config.BokuNoFile) {
            me.addPart("nofile", Utils.sb(1));
        }

        me.addPart("name", Utils.sb(""));
        me.addPart("link", Utils.sb(""));
    }
}
