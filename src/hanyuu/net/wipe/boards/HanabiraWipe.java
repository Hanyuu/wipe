/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.net.wipe.boards;

import hanyuu.net.wipe.Wipe;
import hanyuu.chan.Chan;
import hanyuu.managers.ThreadManager;
import hanyuu.net.proxy.HttpProxy;
import config.Config;
import utils.Utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.ContentBody;
import hanyuu.net.wipe.Action;
/**
 *
 * @author Hanyuu Furude
 * Not ready now;
 */
public class HanabiraWipe extends Wipe {

    public HanabiraWipe(HttpProxy proxy, ThreadManager tm, Chan chan) {
        super(proxy, tm, chan);
    }

    @Override
    public InputStream getCaptchaStream() {
        if (!work) {
            return null;
        }
        lastAction=Action.RequestCaptcha;
        String url;
        txtCaptcha = "";
        url = chan.ChanHost + "/" + chan.CaptchaPath + Config.board + String.valueOf(random.nextInt(Integer.MAX_VALUE)) + ".png";
        headers = new ArrayList<String>();
        ui.logInfo("Request CAPTCHA from " + chan.ChanHost + " via " + proxy);
        switch (Config.workMode) {
            case WipeBoard:
                headers.add("Referer," + chan.ChanHost + Config.board);
                break;
            default:
                headers.add("Referer," + chan.ChanHost + Config.board + "res/" + thread + ".xhtml");
                break;
        }
        return httpClient.getInputStream(url, false);
    }

    @Override
    @SuppressWarnings("CallToThreadDumpStack")
    public void makeMimesAndHeaders(String url) {

            thread = Config.thread;
            //url = chan.ChanHost + Config.board.replace("//", "") + "makeMimesAndHeaders?X-Progress-ID=" + String.valueOf(random.nextInt(Integer.MAX_VALUE)) + "000000";
            url = "http://dobrochan.ru" + Config.board + "post/new.xhtml".replace("//", "");
            //System.out.println(url);

            switch (Config.workMode) {
                /*case OnZeroPage:
                    ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".xhtml");
                    break;*/
                case WipeBoard:
                    headers.add("Referer," + chan.ChanHost + Config.board);
                    me.addPart("thread_id", Utils.sb(0));
                    ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board);
                    break;
                case Force:
                    thread = getNextThread(getThreads());
                    me.addPart("thread_id", Utils.sb(thread));
                    headers.add("Referer," + chan.ChanHost + Config.board + thread + ".xhtml");
                    ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".xhtml");
                    break;
                case WipeThread:
                default:
                    me.addPart("thread_id", Utils.sb(thread));
                    headers.add("Referer," + chan.ChanHost + Config.board + thread + ".xhtml");
                    ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".xhtml");
                    break;
            }

            ui.logInfo("Request CAPTCHA from " + chan.ChanHost + " via " + proxy);

            me.addPart("task", Utils.sb("post"));
            me.addPart("name", Utils.sb(Config.name));

            /*if (Config.sage) {
                me.addPart("sage", Utils.sb("checked"));
            }*/
            me.addPart("subject", Utils.sb(Config.theme));
            me.addPart("message", Utils.sb(msg));

            me.addPart("password", Utils.sb(Config.password));

            me.addPart("post_files_count", Utils.sb("1"));
            me.addPart("file_1_rating", Utils.sb("SFW"));
            ContentBody cb = new FileBody(picture, "image/" + picture.format);
            me.addPart("file_1", cb);

            me.addPart("goto", Utils.sb("board"));

            me.addPart("board", Utils.sb(Config.board));

            if (!work) {
                tm.destroyThread(this, " stopped.");
            }
            httpClient.addCookie("wakabastyle=Futaba");
            httpClient.addCookie("settings=1287054997050");

    }
}
