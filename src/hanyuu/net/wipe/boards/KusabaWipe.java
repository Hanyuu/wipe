/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.net.wipe.boards;

import hanyuu.net.wipe.Wipe;
import hanyuu.net.proxy.HttpProxy;
import config.Config;
import hanyuu.managers.ThreadManager;
import hanyuu.chan.Chan;
import utils.Utils;
import java.io.InputStream;
import java.util.ArrayList;
import hanyuu.net.wipe.Action;
import java.io.BufferedReader;

/**
 *
 * @author Hanyuu Furude
 */
public class KusabaWipe extends Wipe {

    public KusabaWipe(HttpProxy proxy, ThreadManager tm, Chan chan) {
        super(proxy, tm, chan);
    }

    @Override
    @SuppressWarnings("CallToThreadDumpStack")
    public void makeMimesAndHeaders(String url) {

        if (!work) {
            return;
        }
        thread = Config.thread;

        switch (Config.workMode) {
            /*case OnZeroPage:
            ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".html");
            break;*/
            case WipeBoard:
                headers.add("Referer," + chan.ChanHost + Config.board);
                ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board);
                break;
            case Force:
                thread = getNextThread(getThreads());
                headers.add("Referer," + chan.ChanHost + Config.board + "res/" + thread + ".html");
                ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".html");
                break;
            case WipeThread:
            default:
                headers.add("Referer," + chan.ChanHost + Config.board + "res/" + thread + ".html");
                ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".html");
                break;
        }

        me.addPart(chan.FieldBoardName, Utils.sb(Config.board.replace("/", "")));

        switch (Config.workMode) {
            case WipeBoard:
                me.addPart(chan.FieldThreadNumber, Utils.sb("0"));
                break;
            default:
                me.addPart(chan.FieldThreadNumber, Utils.sb(thread));
                break;
        }
    }

    @Override
    public InputStream getCaptchaStream() {
        if (!work) {
            return null;
        }
        lastAction = Action.RequestCaptcha;
        String url;
        txtCaptcha = "";
        headers = new ArrayList<String>();
        url = chan.CaptchaPath + "?0." + String.valueOf(random.nextInt(Integer.MAX_VALUE));
        ui.logInfo("Request CAPTCHA from " + chan.ChanHost + " via " + proxy);
        switch (Config.workMode) {
            case WipeBoard:
                headers.add("Referer," + chan.ChanHost + Config.board);
                break;
            default:
                headers.add("Referer," + chan.ChanHost + Config.board + "res/" + thread + ".html");
                break;

        }
        return httpClient.getInputStream(url, null, headers, true);

    }

    private static void p(Object o) {
        System.out.println(o);
    }
}
