/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.net.wipe.boards;

import hanyuu.net.wipe.Wipe;

import hanyuu.net.proxy.HttpProxy;
import hanyuu.managers.ThreadManager;
import hanyuu.chan.Chan;
import utils.Utils;
import config.Config;
import java.io.InputStream;
import hanyuu.net.wipe.Action;
/**
 *
 * @author Hanyuu Furude
 */
public class FutabaWipe extends Wipe {

    public FutabaWipe(HttpProxy proxy, ThreadManager tm, Chan chan) {
        super(proxy, tm, chan);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void makeMimesAndHeaders(String url) {
            if (!work) {
                return;
            }

            if (onZeroPage()) {
                return;
            }
            switch (Config.workMode) {
                /*case OnZeroPage:
                    ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".html");
                    break;*/
                case WipeBoard:
                    me.addPart(chan.FieldThreadNumber, Utils.sb(0));
                    me.addPart("resto", Utils.sb(thread));
                    ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board);
                    break;
                case Force:
                    thread = getNextThread(getThreads());
                    ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".html");
                    break;
                case WipeThread:
                default:
                    ui.logInfo(wipeThread.getName() + " posting to " + chan.ChanHost + Config.board + "res/" + thread + ".html");
                    me.addPart(chan.FieldThreadNumber, Utils.sb(thread));
                    break;
            }
            me.addPart(chan.task, Utils.sb(chan.post));
            me.addPart("MAX_FILE_SIZE", Utils.sb(2097152));

            ocr.recognizeCaptcha(this);
            me.addPart(chan.FieldCaptcha, Utils.sb(txtCaptcha));
            me.addPart("mode", Utils.sb("regist"));
    }

    public InputStream getCaptchaStream() {
        lastAction=Action.RequestCaptcha;
        return null;
    }
}
