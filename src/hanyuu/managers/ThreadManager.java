/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.managers;

import hanyuu.managers.pictures.ImgManager;
import hanyuu.net.wipe.AbstractWipe;
import hanyuu.net.wipe.boards.WakabaWipe;
import hanyuu.net.wipe.boards.KusabaWipe;
import hanyuu.net.wipe.boards.HanabiraWipe;
import hanyuu.net.wipe.boards.FutabaWipe;

import hanyuu.parser.HornedParser;
import hanyuu.net.proxy.ProxyManager;
import hanyuu.net.proxy.HttpProxy;
import utils.Constants;

import hanyuu.chan.Chan;
import hanyuu.chan.ChanManager;

import config.Config;


import java.io.PrintStream;
import java.io.File;
import java.util.ArrayList;

import hanyuu.ext.ScriptManager;
import hanyuu.net.wipe.Action;

/**
 *
 * @author Hanyuu Furude
 */
public class ThreadManager implements Runnable, Constants {

    private static ThreadManager _instance = null;
    private boolean work = false;
    private ProxyManager pmr;
    private ImgManager im;
    private UIManager uim;
    private Thread ManagerThread;
    private ChanManager cm;
    private CopyPasteManager cpm;
    private boolean console;
    private HornedParser parser;
    private MessageManager mm;
    private ScriptManager scripts;
    private ArrayList<AbstractWipe> notReady = new ArrayList<AbstractWipe>();
    private ArrayList<AbstractWipe> running = new ArrayList<AbstractWipe>();

    public static ThreadManager getInstance() {
        return _instance;
    }

    public boolean isWork() {
        return work;
    }

    @SuppressWarnings({"LeakingThisInConstructor", "CallToThreadStartDuringObjectConstruction"})
    public ThreadManager(boolean console) {
        _instance = this;
        ManagerThread = new Thread(this, "Thread Manager");
        this.console = console;
        ManagerThread.start();
    }

    public synchronized void handleError(AbstractWipe wipe) {
        if (!Config.smartErrorHandler) {
            return;
        }
        uim.getUI().logError("Called handleError() method in thread: [ " + wipe.toString() + " ]" + " Last action is: [ " + wipe.getLastAction() + " ] error is: " + wipe.getException().toString());
        wipe.incErrors();
        if (wipe.getErrors() == Config.smartErrorCount) {
            if (Config.smartErrorAction == 0) {
                wipe.setError(true);
                destroyThread(wipe, "Too many errors, see logs for details.");
                return;
            } else if (Config.smartErrorAction == 1) {
                uim.getUI().logInfo("Too many errors in thread [ " + wipe.toString() + " ], restarting...");
                reStart(wipe);
            }
        }

        switch (wipe.getLastAction()) {

            case RequestCaptcha:
            case Recognition:
                wipe.getOCR().recognizeCaptcha(wipe);
                break;
            case RequestThreads:
                wipe.getThreads();
                break;
            case Posting:
            case Sleeping:
                wipe.post();
                break;
            case DeletingPost:
                //wipe.delete(wipe.getMessages());
                break;
        }
    }

    @Override
    @SuppressWarnings({"CallToThreadDumpStack", "empty-statement"})
    public void run() {

        ManagerThread.setPriority(Thread.MAX_PRIORITY);
        try {

            System.setOut(new PrintStream(System.out, true, Config.EncodingConsole));
            System.setErr(new PrintStream(System.err, true, Config.EncodingConsole));

            pmr = new ProxyManager();
            cpm = new CopyPasteManager();
            cm = new ChanManager();
            cm.createAndLoad();
            Config.useGui = !console;
            scripts = new ScriptManager(this);

            im = new ImgManager(Config.path, this);
            uim = new UIManager(this);
            parser = new HornedParser();
            mm = new MessageManager(this);
            if (console) {
                StartStopManage();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public MessageManager getMM() {
        return mm;
    }

    public HornedParser getParser() {
        return parser;
    }

    public ProxyManager getProxyManager() {
        return pmr;
    }

    public void deleteAllMessages() {
        /*if (work) {
        get(0).deletPost(getAllMessages());
        } else {
        uim.getUI().showMessage("Потоки остановленны.", 0);
        }*/
    }

    public ScriptManager getScripts() {
        return scripts;
    }

    public ImgManager getImgManager() {
        return im;
    }

    public void reBuildImgManager() {
        im = new ImgManager(Config.path, this);
    }

    public UIManager getUIManager() {
        return uim;
    }

    public ChanManager getChanManager() {
        return cm;
    }

    public CopyPasteManager getCopyPasteManager() {
        return cpm;
    }

    public void sleepWipe(long time, AbstractWipe wipe) {

        try {
            if (wipe.isSleeping()) {
                return;
            }
            wipe.setLastAction(Action.Sleeping);
            wipe.setSleeping(true);
            synchronized (wipe) {
                wipe.wait(time);
            }
            wipe.setSleeping(false);
        } catch (Exception e) {
            wipe.setSleeping(false);
            wipe.setException(e);
            handleError(wipe);
        }
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void StartStopManage() {
        try {
            if (!new File(Config.path).exists() && !Config.BokuNoFile) {
                uim.getUI().showMessage("File or folder " + Config.path + " not found", 0);
                uim.getUI().logError("File or folder " + Config.path + " not found");
                return;
            }

            if (work) {
                work = false;
                uim.getUI().SwitchStartStop();

                for (int i = 0; i < running.size();) {
                    destroyThread(running.get(i), "Stopped.");
                    i++;
                }
            } else {
                if (Config.chanName.contains("NoName")) {
                    uim.getUI().logError("No chan selected");
                    return;
                }
                work = true;
                uim.getUI().SwitchStartStop();
                int threads = uim.getUI().getThreads();
                HttpProxy[] proxys;
                if (Config.noProxy) {
                    proxys = new HttpProxy[1];
                    proxys[0] = new HttpProxy("no proxy", -1);
                    threads = 1;
                } else {
                    proxys = pmr.toArray(new HttpProxy[0]);
                }
                for (int i = 0; i < threads; i++) {
                    runWipe(proxys[i]);
                }
                uim.getUI().setTitle("Рогатулечка. Чан: " + Config.chanName + ", Доска: " + Config.board + ". Потоков: " + running.size());
            }
        } catch (Exception e) {
            e.printStackTrace();
            work = false;
            uim.getUI().SwitchStartStop();
            uim.getUI().showMessage("Ошибка при запуске.\n" + e.toString(), 0);
        }

    }

    public boolean isReady() {
        return notReady.isEmpty();
    }

    public void removeFromNotReady(AbstractWipe wipe) {
        notReady.remove(wipe);
        if (running.contains(wipe)) {
            running.remove(wipe);
        }
        running.add(wipe);
        this.uim.getUI().logInfo("Ready threads: " + running.size());
        /*//System.out.println(notReady.size());
        if (notReady.isEmpty()) {
        if (running.isEmpty()) {
        uim.getUI().showMessage("Ошибка! Ни одного \"готового\" потока нету.", 0);
        StartStopManage();
        return;
        }
        
        for (AbstractWipe w : running) {
        uim.getUI().logInfo("Notifyng thread [ " + w.toString() + " ] ...");
        w.myNotify();
        }
        
        }*/
    }

    public void runThreads() {

        for (AbstractWipe w : running) {
            uim.getUI().logInfo("Notifyng thread [ " + w.toString() + " ] ...");
            w.myNotify();

        }
        running.clear();
    }

    public void runWipe(HttpProxy p) {
        AbstractWipe ww = null;
        Chan chan = cm.getByName(Config.chanName);
        if (chan == null || chan.ChanType == null) {
            uim.getUI().logError("unknown Chan: " + Config.chanName + " type: " + chan.ChanType.name());
            uim.getUI().SwitchStartStop();
            return;
        }
        switch (chan.ChanType) {
            case Wakaba:
                ww = new WakabaWipe(p, this, chan);
                break;
            case Kusaba:
                ww = new KusabaWipe(p, this, chan);
                break;
            case Hanabira:
                ww = new HanabiraWipe(p, this, chan);
                break;
            case Futaba:
                ww = new FutabaWipe(p, this, chan);
                break;
        }
        if (ww != null) {
            if (Config.waitForNotReady) {
                notReady.add(ww);
            } else {
                running.add(ww);
            }
        } else {
            uim.getUI().logError("Unknown Chan: " + Config.chanName + " type: " + chan.ChanType.name());
            uim.getUI().SwitchStartStop();
            return;
        }
    }

    public synchronized int getAllSuccessful() {
        int i = 0;
        for (int l = 0; l < running.size(); l++) {
            i += running.get(l).getSuccessful();
        }
        return i;
    }

    public synchronized int getAllFailed() {
        int i = 0;
        for (int l = 0; l < running.size(); l++) {
            i += running.get(l).getFailed();
        }
        return i;
    }

    public int size() {
        return running.size();
    }

    public void reStart(AbstractWipe ww) {
        running.remove(ww);
        switch (ww.getChan().ChanType) {
            case Wakaba:
                ww = new WakabaWipe(ww.getProxy(), this, ww.getChan());
                break;
            case Kusaba:
                ww = new KusabaWipe(ww.getProxy(), this, ww.getChan());
                break;
            case Hanabira:
                ww = new HanabiraWipe(ww.getProxy(), this, ww.getChan());
                break;
            case Futaba:
                ww = new FutabaWipe(ww.getProxy(), this, ww.getChan());
                break;
        }
        running.add(ww);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public void destroyThread(AbstractWipe ww, String reason) {

        try {
            synchronized (lock) {
                if (ww == null) {
                    return;
                }
                /*
                if ((!Config.noProxy && ww.getProxy() != null) && reason.contains(ww.getProxy().getHost())) {
                getProxyManager().delete(ww.getProxy(), uim.getUI());
                }*/

                ww.myNotify();
                ww.stop();


                if (!running.contains(ww) || !notReady.contains(ww)) {
                    return;
                }
                if (!running.remove(ww)) {
                    notReady.remove(ww);
                }
                work = !(running.isEmpty());
                uim.getUI().SwitchStartStop();
                uim.getTray().switchState();

                if ((Config.useTmp || Config.randomPicGenerate) && ww.getFile().exists()) {
                    ww.getFile().delete();
                }
                if (work) {
                    uim.getUI().logInfo("Threads count is: " + running.size());
                }

                uim.getUI().setTitle("Рогатулечка. Чан: " + Config.chanName + ", Доска: " + Config.board + ". Потоков: " + running.size());
                if (!work) {
                    uim.getUI().setTitle("Рогатулечка. Чан: " + Config.chanName + ", Доска: " + Config.board + ".");
                }
                if (ww.isError()) {
                    uim.getUI().logError("Thread [" + ww.getThread().getName() + "] stoped by ThreadManager.\nReason: " + reason);
                    if (Config.useGui) {
                        uim.getTray().trayPrintError("Ханюша", "Поток " + ww.getThread().getName() + "\nЗавершён причина: " + reason);
                    }
                } else {

                    if (reason != null && reason.isEmpty()) {
                        uim.getUI().logInfo("Thread [" + ww + "] stoped.");
                    } else {
                        uim.getUI().logInfo("Thread [" + ww + "] stoped.\nReason: " + reason);
                    }
                    if (reason != null && reason.isEmpty()) {
                        uim.getTray().trayPrint("Ханюша", "Поток " + ww + " завершён.");
                    } else {
                        uim.getTray().trayPrint("Ханюша", "Поток " + ww + " завершён.\nПричина: " + reason);
                    }
                }

                ww = null;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
