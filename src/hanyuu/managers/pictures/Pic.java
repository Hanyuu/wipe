/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.managers.pictures;

import hanyuu.managers.ThreadManager;
import java.io.File;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

/**
 *
 * @author Hanyuu
 */
public class Pic extends File {

    public int w_i;
    public int h_i;
    public String w_s;
    public String h_s;
    public BufferedImage bi;
    public String format;
    @SuppressWarnings({"CallToThreadDumpStack", "LeakingThisInConstructor"})
    public Pic(String name) {
        super(name);
        createBufferedImage(this);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    public Pic(File f) {
        super(f.getAbsolutePath());
        format=getName().substring(getName().indexOf(".") + 1).toLowerCase();
        createBufferedImage(f);
    }

    @SuppressWarnings("CallToThreadDumpStack")
    private void createBufferedImage(File f) {
        try {
            bi = ImageIO.read(f);
            if(bi==null){
                this.createBufferedImage(ThreadManager.getInstance().getImgManager().getFile());
                /*ThreadManager.getInstance().getUIManager().getUI()
                        .showMessage(f.getAbsolutePath()+"\nНеявляется изображением!", 0);
                ThreadManager.getInstance().StartStopManage();*/
            }
            w_i = bi.getWidth();
            h_i = bi.getHeight();
            w_s=String.valueOf(w_i);
            h_s=String.valueOf(h_i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
