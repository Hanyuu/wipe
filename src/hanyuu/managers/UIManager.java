/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hanyuu.managers;

import ui.interfaces.UI;
import ui.interfaces.ITray;

import ui.cli.CLI;
import ui.cli.CTray;
import ui.gui.GUI;
import ui.gui.Tray;


import hanyuu.chan.Chan;
import hanyuu.ext.interfaces.OCR;

import config.Config;
import hanyuu.net.proxy.HttpProxy;
import hanyuu.ext.ScriptContainer;

/**
 *
 * @author Hanyuu Furude
 */
public class UIManager {

    private UI ui;
    private ITray tray;
    private ThreadManager tm;

    @SuppressWarnings("CallToThreadDumpStack")
    public UIManager(ThreadManager tm) {
        this.tm = tm;
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void init() throws Exception {
        if (Config.useGui) {
            System.out.println("Starting GUI");
            this.ui = new GUI(tm);
            this.tray = new Tray(tm);
            tray.setUI(ui);
            tray.addIcon();
            for (Chan ca : tm.getChanManager()) {
                ui.addChan(ca.ChanName);
            }
            for (HttpProxy p : tm.getProxyManager()) {
                ui.addProxy(p);
            }
            for (ScriptContainer s : tm.getScripts().getScripts()) {
                ui.addScript(s);
            }
            for (OCR ocr : tm.getScripts().getOCRs().values()) {
                ui.addOCR(ocr);
            }
            ui.reSelectChan();
            ui.reSelectOCR();
            ui.setVisible(true);
        } else {
            this.ui = new CLI(tm);
            this.tray = new CTray();
        }

        ui.logInfo("Loaded " + tm.getProxyManager().size() + " proxys");
        ui.logInfo("Loaded: " + tm.getCopyPasteManager().size() + " copypaste blocks");
        System.gc();
    }

    public UI getUI() {
        return ui;
    }

    public ITray getTray() {
        return tray;
    }
}
